# Pure Iso-Type Systems
Paper and supplementary materials

https://bitbucket.org/ypyang/pits-jfp

## Paper 
See [print.pdf](print.pdf).

## Coq formalization of proofs
See [coq/README.md](coq/README.md). Files are located at [coq/](coq/).

## Implementation
See [impl/README.md](impl/README.md). Source files are located at [impl/](impl/).
Try the in-browser compiler [online](https://fun-lang.github.io/).
