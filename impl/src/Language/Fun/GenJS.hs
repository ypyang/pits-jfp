module Language.Fun.GenJS (
  jsGen,
  ) where


import           Language.Fun.CodeGen
import qualified Language.Fun.Core            as C hiding (M, runM)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import qualified Language.Fun.Typecheck       as T hiding (M, runM)
import           Prelude                      hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen hiding (Pretty)

jsOpClass :: Doc
jsOpClass = text "$o"

jsRawClass :: Doc
jsRawClass = text "new $r"

jsUnaryOp :: LitUnaryOp -> Doc
jsUnaryOp op = case op of
  LOpShow -> text "show"
  LOpNeg -> text "neg"
  LOpNot -> text "not"

jsBinOp :: LitBinOp -> Doc
jsBinOp op = case op of
  LOpAdd -> text "plus"
  LOpMinus -> text "minus"
  LOpMul -> text "mul"
  LOpConcat -> text "concat"
  LOpG -> text "grt"
  LOpGE -> text "grteq"
  LOpL -> text "less"
  LOpLE -> text "lesseq"
  LOpEq -> text "eq"
  LOpNeq -> text "neq"
  LOpAnd -> text "and"
  LOpOr -> text "or"

jsDocGenFunc :: [Id] -> Doc -> Doc
jsDocGenFunc xs d =
  let xs' = map jsName xs
      arity = int (length xs)
  in
  parens (text "function(){var self=function" <> parens
  (strsep "," (map (\v -> if null v then text "$x" else text v) xs')) <>
  braces (text "return arguments.length<" <> arity <>
          text "?new $i(self,arguments):$p(" <> parens d <>
          text ",arguments," <> arity <> text ");") <>
  text ";return self;}") <> text "()"

jsGenStr :: MTm -> Doc
jsGenStr = text . show . pp

jsGenRaw :: MTm -> Doc
jsGenRaw t = jsRawClass <> parens (text . show . show . pp $ t)

jsDocGenOp :: Doc -> Doc -> Doc
jsDocGenOp op d = jsOpClass <> dot <>
  op <> dot <> text "app" <> parens d

jsGenOp :: Doc -> [MTm] -> Doc
jsGenOp op es = jsDocGenOp op (strsep "," (map jsGenTm es))

jsGenBnd :: (Id, MTm) -> Doc
jsGenBnd (x, e) = text "var" <+> text (jsName x) <> equals <>
  jsGenTm e <> char ';'

jsGenTm :: MTm -> Doc
jsGenTm inp = case inp of
  MVar x -> text (jsName x)
  MStar -> jsGenRaw inp
  MApp f es -> jsGenTm f <> dot <> text "app" <>
    parens (strsep "," (map jsGenTm es))
  MLam xs e -> parens (jsDocGenFunc xs (jsGenTm e))
  MPi{} -> jsGenRaw inp
  MMu x e -> jsDocGenOp (text "fix") $
    jsGenTm (MLam [x] e)
  MAny -> jsGenRaw inp
  MUnit -> text "null"
  MLet bnds e2 -> parens (text "function" <> parens empty <>
                          braces (
                             strsep "" (map (\(x, e) ->
                                               text "var" <+> text (jsName x) <> equals <>
                                               text "$fi" <> parens (jsGenTm e) <> char ';') bnds) <>
                             text "return" <+> jsGenTm e2 <> char ';'
                             )) <> parens empty
  MIf e1 e2 e3 -> jsGenOp (text "ifthenelse") [e1, e2, e3]
  MLTy{} -> jsGenRaw inp
  MLVal (LVBool x) -> text $ if x then "true" else "false"
  MLVal{} -> jsGenStr inp
  MLUnary op e -> jsGenOp (jsUnaryOp op) [e]
  MLBin op e1 e2 -> jsGenOp (jsBinOp op) [e1, e2]

jsGen :: T.Ctx -> C.Tm -> M (Doc, Doc, Doc)
jsGen ctx tm = do
  bnds <- eraseCtx ctx
  e <- eraseTm tm
  let genBnd (Bnd x e2) = jsGenBnd (x, (liftTm e2))
  return (strsep "" (map genBnd bnds), jsGenTm (liftTm e),
          text "/*" <>
          (if null bnds then empty
           else line <> strvsep ";" (map pp bnds)) <> text " ;" <$>
          pp e <$> text "*/" <> line)
