module Language.Fun.CodeGen (
  M,
  runM,
  Id(..),
  Tm(..),
  Bnd(..),
  varstr,
  toId,
  bindRep,
  eraseTm,
  eraseCtx,
  bndsToTm,
  MTm(..),
  liftTm,
  ) where


import           Control.Applicative              ((<*>))
import           Control.Monad.Identity
import           Data.Char                        (isUpper)
import qualified Data.LinkedHashMap.IntMap        as HM
import qualified Language.Fun.Core                as C hiding (M, runM)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import qualified Language.Fun.Typecheck           as T hiding (M, runM)
import           Prelude                          hiding ((<$>), (<*>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (bind, subst)

-- Erased term
data Id = Id {
  jsName :: String,
  hsName :: String
  } deriving (Eq, Show)

data Tm = Var Id
        | Star
        | App Tm Tm
        | Lam Id Tm
        | Pi Id Tm Tm
        | Mu Id Tm
        | Any
        | Unit
        | Let Bnd Tm
        | IfThenElse Tm Tm Tm
        | LTy LitTy
        | LVal LitVal
        | LUnary LitUnaryOp Tm
        | LBin LitBinOp Tm Tm
        deriving (Eq, Show)

data MTm = MVar Id
         | MStar
         | MApp MTm [MTm]
         | MLam [Id] MTm
         | MPi [(Id, MTm)] MTm
         | MMu Id MTm
         | MAny
         | MUnit
         | MLet [(Id, MTm)] MTm
         | MIf MTm MTm MTm
         | MLTy LitTy
         | MLVal LitVal
         | MLUnary LitUnaryOp MTm
         | MLBin LitBinOp MTm MTm
         deriving (Eq, Show)

data Bnd = Bnd Id Tm deriving (Eq, Show)

clusterLam :: Tm -> ([String], Tm)
clusterLam (Lam x e) =
  let (vars, body) = clusterLam e
      v = jsName x
  in ((if null v then "_" else v) :vars, body)
clusterLam e = ([], e)

liftTm :: Tm -> MTm
liftTm inp = case inp of
  Var n -> MVar n
  Star -> MStar
  App{} -> let (f, es) = mApp inp
           in MApp f (reverse es)
  Lam{} -> let (xs, e') = mLam inp
           in MLam xs e'
  Pi{} -> let (bs, t2') = mPi inp
          in MPi bs t2'
  Mu x e -> MMu x (liftTm e)
  Any -> MAny
  Unit -> MUnit
  Let{} -> let (bs, t2') = mLet inp
           in MLet bs t2'
  IfThenElse t1 t2 t3 -> MIf (liftTm t1) (liftTm t2) (liftTm t3)
  LTy t -> MLTy t
  LVal v -> MLVal v
  LUnary op e -> MLUnary op (liftTm e)
  LBin op e1 e2 -> MLBin op (liftTm e1) (liftTm e2)
  where 
    mApp (App e1 e2) =
      let (f, es) = mApp e1
      in (f, liftTm e2 : es)
    mApp e = (liftTm e, [])

    mLam (Lam x e) =
      let (xs, e') = mLam e
      in (x : xs, e')
    mLam e = ([], liftTm e)

    mPi (Pi x t1 t2) =
      let (bs, t2') = mPi t2
      in ((x, liftTm t1) : bs, t2')
    mPi e = ([], liftTm e)

    mLet (Let (Bnd x e1) e2) =
      let (bs, e2') = mLet e2
      in ((x, liftTm e1) : bs, e2')
    mLet e = ([], liftTm e)

instance Pretty MTm where
  ppr p inp = case inp of
    MVar x -> text (jsName x)
    MStar -> char '*'
    MApp f es -> parensIf (p > 1) $ ppr 1 f <+> strsep " " (map (ppr 2) es)
    MLam xs e -> let xs' = map jsName xs
                     bndDoc = strsep " "
                       (map (\v -> text $ if null v then "_" else v) xs')
                 in parensIf (p > 0) $ backslash <> bndDoc <+> dot <+> pp e
    MPi bnds t2 -> let lDoc (x, t1) =
                         (if null (jsName x) then ppr 1 t1
                         else parens (text (jsName x) <+> colon <+> pp t1))
                         <+> text "->"
                   in parensIf (p > 0) $ strsep " " (map lDoc bnds)
                      <+> pp t2
    MMu x e -> parensIf (p > 0) $ char '/' <> text (jsName x) <+> dot <+> pp e
    MAny -> char '_'
    MUnit -> text "()"
    MLet bnds e2 ->  parensIf (p > 0) $
      strsep " " (map (\(x, e1) -> keyword "let" <+> text (jsName x)
                                   <+> equals <+> ppr p e1 <+>
                                   keyword "in") bnds) <+> pp e2
    MIf e1 e2 e3 -> parensIf (p > 0) $
      keyword "if" <+> pp e1 <+> keyword "then" <+> pp e2 <+>
      keyword "else" <+> pp e3
    MLTy ty -> pp ty
    MLVal val -> pp val
    MLUnary op a -> parensIf (p > 0) $ pp op <> parens (pp a)
    MLBin op a b ->
      let ad = ppr (binPrec op) a
          bd = ppr (succ . binPrec $ op) b
      in parensIf (p > 0) $ ad <+> pp op <+> bd

instance Pretty Tm where
  ppr p inp = case inp of
    Var x -> text (jsName x)
    Star -> char '*'
    App e1 e2 -> parensIf (p > 1) $ ppr 1 e1 <+> ppr 2 e2
    Lam{} -> let (xs', e') = clusterLam inp
                 bndDoc = strsep " " (map text xs')
             in parensIf (p > 0) $ backslash <> bndDoc <+> dot <+> pp e'
    Pi x t1 t2 -> let lDoc = if null (jsName x) then ppr 1 t1
                             else parens (text (jsName x) <+> colon <+> pp t1)
                  in parensIf (p > 0) $ lDoc <+> text "->" <+> pp t2
    Mu x e -> parensIf (p > 0) $ char '/' <> text (jsName x) <+> dot <+> pp e
    Any -> char '_'
    Unit -> text "()"
    Let (Bnd x e1) e2 -> parensIf (p > 0) $
      keyword "let" <+> text (jsName x) <+> equals <+> ppr p e1 <+>
      keyword "in" <+> pp e2
    IfThenElse e1 e2 e3 -> parensIf (p > 0) $
      keyword "if" <+> pp e1 <+> keyword "then" <+> pp e2 <+>
      keyword "else" <+> pp e3
    LTy ty -> pp ty
    LVal val -> pp val
    LUnary op a -> parensIf (p > 0) $ pp op <> parens (pp a)
    LBin op a b ->
      let ad = ppr (binPrec op) a
          bd = ppr (succ . binPrec $ op) b
      in parensIf (p > 0) $ ad <+> pp op <+> bd

instance Pretty Bnd where
  ppr p (Bnd x e) = keyword "def" <+> text (jsName x) <+> equals <+> ppr p e

type M = FreshMT Identity

varstr :: C.VarName -> String
varstr = name2String

runM :: M a -> a
runM = runIdentity . runFreshMT

bindRep :: (Tm -> Tm) -> Int -> Tm -> Tm
bindRep _ 0 e = e
bindRep f n e = f (bindRep f (n - 1) e)

toId :: String -> Id
toId v = Id { jsName = map (\c -> if c == '\'' then '$' else c) v,
              hsName = if null v then v
                       else if isUpper (head v)
                            then "_ctor_" ++ v else v }

bndsToTm :: [Bnd] -> Tm -> Tm
bndsToTm bs e = foldr Let e bs

eraseTm :: C.Tm -> M Tm
eraseTm inp = case inp of
  C.Var v -> return $ Var (toId $ varstr v)
  C.Star -> return Star
  C.App e1 e2 -> App `fmap` eraseTm e1 <*> eraseTm e2
  C.Lam b -> do
    ((x, _), e) <- unbind b
    Lam (toId $ varstr x) `fmap` eraseTm e
  C.Pi b -> do
    ((x, Embed t1), t2) <- unbind b
    Pi (toId $ varstr x) `fmap` eraseTm t1 <*> eraseTm t2
  C.Mu b -> do
    ((x, _), e) <- unbind b
    Mu (toId $ varstr x) `fmap` eraseTm e
  C.Cast _ _ _ _ e -> eraseTm e
  C.ConPi _ t -> eraseTm t
  C.ConLam _ e -> eraseTm e
  C.ConApp _ e -> eraseTm e
  C.Any -> return Any
  C.Unit -> return Unit
  C.Let b -> do
    ((x, Embed e1), e2) <- unbind b
    e1' <- eraseTm e1
    e2' <- eraseTm e2
    return $ Let (Bnd (toId $ varstr x) e1') e2'
  C.IfThenElse e1 e2 e3 ->
    IfThenElse `fmap` eraseTm e1 <*> eraseTm e2 <*> eraseTm e3
  C.LTy t -> return $ LTy t
  C.LVal v -> return $ LVal v
  C.LUnary op e -> LUnary op `fmap` eraseTm e
  C.LBin op e1 e2 -> LBin op `fmap` eraseTm e1 <*> eraseTm e2

eraseCtx :: T.Ctx -> M [Bnd]
eraseCtx ctx = mapM (\(v, e) ->
                      do e' <- eraseTm e
                         return $ Bnd (toId $ varstr v) e') (HM.toList $ T.bndCtx ctx)
