module Language.Fun.Pretty (
  Pretty(..),
  parensIf,
  keyword,
  docsep,
  strsep,
  strspsep,
  docvsep,
  strvsep,
  warn,
  info,
  resDoc,
  tmpName,
  tmpNameList,
  ) where

import           Prelude                      hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen hiding (Pretty)

class (Show p) => Pretty p where
  ppr :: Int -> p -> Doc

  ppraw :: p -> Doc
  ppraw = text . show

  pp :: p -> Doc
  pp = ppr 0

parensIf ::  Bool -> Doc -> Doc
parensIf True = parens
parensIf False = id

docsep :: Doc -> [Doc] -> Doc
docsep _ [] = empty
docsep _ [d] = d
docsep s (d:ds) = d <> s <> docsep s ds

strsep :: String -> [Doc] -> Doc
strsep s = docsep (text s)

strspsep :: String -> [Doc] -> Doc
strspsep s = docsep (if null s then space else
                       space <> text s <> space)

docvsep :: Doc -> [Doc] -> Doc
docvsep d = docsep (space <> d <> line)

strvsep :: String -> [Doc] -> Doc
strvsep s = docvsep (text s)

keyword :: String -> Doc
keyword = bold . text

warn :: String -> Doc
warn s = dullred . bold $ text "***" <+> text s <> colon

info :: String -> Doc
info = dullyellow . bold . brackets . text

resDoc :: Doc -> Doc
resDoc = bold

tmpName :: String -> String
tmpName x = '_':x

tmpNameList :: String -> [String]
tmpNameList s = map (\x -> tmpName (s ++ show x)) [(0 :: Int)..]

