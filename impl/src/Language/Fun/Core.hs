{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Fun.Core (
  Ty,
  Tm(..),
  VarName,
  Con(..),
  CastTy(..),
  vnbind,
  Tele,
  clusterLam,
  clusterPi,
  flattenApp,
  teleToBind,
  substVec,
  M,
  runM,
  clusterConPi,
  clusterConLam,
  vardoc,
  mkConPi,
  mkConLam,
  mkConApp,
  CastAnn(..),
  vnSubst,
  tmpVar,
  tmpVarWith,
  dummyvar,
  isDummyVar,
  tmpVarList,
  isFvOf,
  Bnd,
  ) where

import           Control.Monad.Identity
import           Data.Function                                  (on)
import           Data.Hashable                                  (Hashable,
                                                                 hashWithSalt)
import qualified Data.Set                                       as S
import           Data.Typeable                                  (Typeable)
import           GHC.Generics
import           Language.Fun.Lit
import           Language.Fun.Pretty
import           Prelude                                        hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen                   hiding (Pretty)
import           Unbound.Generics.LocallyNameless
import           Unbound.Generics.LocallyNameless.Internal.Fold (toListOf)

-- Unbound library
type VarName = Name Tm
type Bnd = Bind (VarName, Embed Ty) Tm
type Tele = (VarName, Ty)

-- Expression
data Con = Eq Ty Ty deriving (Show, Generic, Typeable)
data CastTy = Up | Down deriving (Show, Generic, Typeable, Eq)
data CastAnn = Weak | Full deriving (Show, Generic, Typeable, Eq)
type Ty = Tm
data Tm = Var VarName
        | Star
          -- Standard syntax
        | App Tm Tm
        | Lam Bnd
        | Pi Bnd
        | Mu Bnd
        | Cast CastTy CastAnn Int (Maybe Ty) Tm
        | ConPi [Con] Ty
        | ConLam [Con] Tm
        | ConApp Int Tm
          -- Extended syntax
        | Any
        | Unit
        | Let Bnd
        | IfThenElse Tm Tm Tm
          -- Literal
        | LTy LitTy
        | LVal LitVal
        | LUnary LitUnaryOp Tm
        | LBin LitBinOp Tm Tm
        deriving (Show, Generic, Typeable)

-- Unbound library instances
instance Alpha Tm where
  aeq' _ Any _ = True
  aeq' _ _ Any = True
  aeq' c a b = ((gaeq c) `on` from) a b

instance Alpha Con
instance Alpha CastTy
instance Alpha CastAnn

instance Subst Tm LitTy
instance Subst Tm LitVal
instance Subst Tm LitBinOp
instance Subst Tm LitUnaryOp
instance Subst Tm Con
instance Subst Tm CastTy
instance Subst Tm CastAnn

instance Subst Tm Tm where
  isvar (Var v) = Just (SubstName v)
  isvar _  = Nothing

instance Hashable (Name Tm) where
  hashWithSalt x v = hashWithSalt x (show v)

-- Utility functions
type M = FreshMT Identity

runM :: M a -> a
runM = runIdentity . runFreshMT

dummyvar :: VarName
dummyvar = string2Name ""

vnbind :: VarName -> Ty -> Tm -> Bnd
vnbind x t = bind (x, Embed t)

isFvOf :: VarName -> Tm -> Bool
isFvOf v e = v `S.member` fvSet e

teleToBind :: (Bnd -> Tm) -> Tm -> [Tele] -> Tm
teleToBind f = foldr (\(v, t) tm -> f (vnbind v t tm))

clusterLam :: Tm -> M ([Tele], Tm)
clusterLam (Lam b) = do
  ((n, Embed t), e) <- unbind b
  (vars, body) <- clusterLam e
  return ((n, t) : vars, body)
clusterLam e = return ([], e)

clusterPi :: Tm -> M ([Tele], Tm)
clusterPi (Pi b) = do
  ((n, Embed t), e) <- unbind b
  (vars, body) <- clusterPi e
  return ((n, t) : vars, body)
clusterPi e = return ([], e)

clusterConPi :: Tm -> ([Con], Tm)
clusterConPi (ConPi c e) =
  let (cons, body) = clusterConPi e
  in (c ++ cons, body)
clusterConPi e = ([], e)

clusterConLam :: Tm -> ([Con], Tm)
clusterConLam (ConLam c e) =
  let (cons, body) = clusterConLam e
  in (c ++ cons, body)
clusterConLam e = ([], e)

flattenApp :: Tm -> [Tm]
flattenApp (App f a) = flattenApp f ++ [a]
flattenApp e = [e]

fvSet :: Tm -> S.Set VarName
fvSet = S.fromList . toListOf fv

vnSubst :: VarName -> Tm -> Tm -> Tm
vnSubst = subst

substVec :: Tm -> [Tele] -> Tm
substVec = foldl (\v (x, e) -> vnSubst x e v)

mkConPi :: [Con] -> Tm -> Tm
mkConPi _  = id

mkConLam :: [Con] -> Tm -> Tm
mkConLam _  = id

mkConApp :: Int -> Tm -> Tm
mkConApp _ = id

tmpVar :: VarName -> VarName
tmpVar = tmpVarWith ""

tmpVarWith :: String -> VarName -> VarName
tmpVarWith x y = string2Name $ tmpName (x ++ name2String y)

tmpVarList :: String -> [VarName]
tmpVarList x = map (\s -> if null s then dummyvar else string2Name s) (tmpNameList x)

isDummyVar :: VarName -> Bool
isDummyVar v = v == dummyvar || null (name2String v)

-- Pretty print
instance Pretty Tm where
  ppr p = runM . pprM p

instance Pretty Con where
  ppr _ = runM . ppMCon

instance Pretty CastAnn where
  ppr _ = runM . ppMCastAnn

vardoc :: VarName -> Doc
vardoc v = if isDummyVar v then char '_' else text (name2String v)

ppM :: Tm -> M Doc
ppM = pprM 0

ppMCon :: Con -> M Doc
ppMCon (Eq t t') = do
  td <- ppM t
  td' <- ppM t'
  return $ td <+> char '~' <+> td'

ppMCastTy :: CastTy -> M Doc
ppMCastTy ty = return . text $ case ty of
  Up -> "castup"
  Down -> "castdn"

ppMCastAnn :: CastAnn -> M Doc
ppMCastAnn ty = return . text $ case ty of
  Weak -> ""
  Full -> "!"

pprMApp :: Int -> Tm -> Tm -> M Doc
pprMApp p a b = do
  ad <- pprM 1 a
  bd <- pprM 2 b
  return . parensIf (p > 1) $ ad <+> bd

pprM :: Int -> Tm -> M Doc
pprM p inp = case inp of
  Var n -> return $ vardoc n
  Star -> return $ char '*'
  App e1@(App (Var v) a) b ->
    case name2String v of
      'O':'p':'(':op' -> do
        let op = init op'
        ad <- pprM (binPrec LOpAnd) a
        bd <- pprM (succ . binPrec $ LOpAnd) b
        return . parensIf (p > 0) $ ad <+> text op <+> bd
      _ -> pprMApp p e1 b
  App a b -> pprMApp p a b
  Lam{} -> clusterLam inp >>= \(vars, body) -> do
    varsDoc <- forM vars $ \(n, t) -> do
      varDoc <- ppM t
      return . parensIf (length vars > 1) $
        vardoc n <+> colon <+> varDoc
    bodyDoc <- ppM body
    return . parensIf (p > 0) $
      backslash <> hsep varsDoc <+> dot <+> bodyDoc
  Pi bnd -> do
    ((n, Embed t1), t2) <- unbind bnd
    firstDoc <- if isDummyVar n || not (n `isFvOf` t2) then pprM 1 t1 -- Non-dependent Pi
                else do d <- ppM t1
                        return $ parens (vardoc n <+> colon <+> d)
    secondDoc <- ppM t2
    return . parensIf (p > 0) $ firstDoc <+> text "->" <+> secondDoc
  Mu bnd -> do
    ((n, Embed t), e) <- unbind bnd
    td <- ppM t
    ed <- ppM e
    return . parensIf (p > 0) $
      char '/' <> vardoc n <+> colon <+> td <+> dot <+> ed
  Cast ty ann s mt e -> if s == 0 then pprM p e else do
    tyd <- ppMCastTy ty
    annd <- ppMCastAnn ann
    ed <- pprM 1 e
    td <- maybe (return empty)
      (\t -> do
          d <- ppM t
          return $ brackets d <> space) mt
    return . parensIf (p > 0) $
      tyd <> annd <> (if s == 1 then empty else
                        char '^' <> (if s < 0 then parens else id) (int s))
      <+> td <> ed
  ConPi{} -> let (cons, body) = clusterConPi inp in do
    consDoc <- do
      ds <- mapM ppMCon cons
      return . parens $ strsep ", " ds
    bodyDoc <- ppM body
    return . parensIf (p > 0) $ consDoc <+> text "=>" <+> bodyDoc
  ConLam{} -> let (cons, body) = clusterConLam inp in do
    consDoc <- do
      ds <- mapM ppMCon cons
      return . parens $ strsep ", " ds
    bodyDoc <- ppM body
    return . parensIf (p > 0) $ text "\\~" <> consDoc <+> dot <+> bodyDoc
  ConApp s e -> do
    ed <- ppM e
    return . parensIf (p > 0) $ char '#' <>
      (if s > 1 then char '^' <> int s else empty) <>
      parens ed

  Any -> return $ char '_'
  Unit -> return $ text "()"
  Let bv -> do
    ((v, Embed tm), e) <- unbind bv
    tmDoc <- ppM tm
    eDoc <- ppM e
    return . parensIf (p > 0) $
      keyword "let" <+> vardoc v <+> equals <+>
      tmDoc <+> keyword "in" <+> eDoc
  IfThenElse e1 e2 e3 -> do
    d1 <- ppM e1
    d2 <- ppM e2
    d3 <- ppM e3
    return . parensIf (p > 0) $
      keyword "if" <+> d1 <+> keyword "then" <+>
      d2 <+> keyword "else" <+> d3

  LTy ty -> return $ pp ty
  LVal val -> return $ pp val
  LUnary op a -> do
    ad <- ppM a
    return . parensIf (p > 0) $ pp op <> parens ad
  LBin op a b -> do
    ad <- pprM (binPrec op) a
    bd <- pprM (succ . binPrec $ op) b
    return . parensIf (p > 0) $ ad <+> pp op <+> bd

