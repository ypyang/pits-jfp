{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
module Language.Fun.Lit (
  LitTy(..),
  LitVal(..),
  LitUnaryOp(..),
  LitBinOp(..),
  binPrec,
  typeOfVal,
  typeOfUOp,
  typeOfBOp,
  ) where

import           Data.Typeable                    (Typeable)
import           GHC.Generics
import           Language.Fun.Pretty
import           Prelude                          hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Text.Printf                      (printf)
import           Unbound.Generics.LocallyNameless

data LitTy = LTInt | LTStr | LTBool | LTAny deriving (Show, Generic, Typeable, Eq)
data LitVal = LVInt Int | LVStr String | LVBool Bool deriving (Show, Generic, Typeable, Eq)
data LitUnaryOp = LOpShow | LOpNeg | LOpNot deriving (Show, Generic, Typeable, Eq)
data LitBinOp = LOpAdd | LOpMinus | LOpMul | LOpConcat
  | LOpG | LOpGE | LOpL | LOpLE | LOpEq | LOpNeq
  | LOpAnd | LOpOr
  deriving (Show, Generic, Typeable, Eq)

instance Alpha LitTy
instance Alpha LitVal
instance Alpha LitUnaryOp
instance Alpha LitBinOp

negSign :: String
negSign = "-"

binPrec :: LitBinOp -> Int
binPrec op = case op of
  LOpMul -> 1
  LOpAdd -> 1
  _ -> 0

typeOfVal :: LitVal -> LitTy
typeOfVal val = case val of
  LVInt{} -> LTInt
  LVStr{} -> LTStr
  LVBool{} -> LTBool

typeOfUOp :: LitUnaryOp -> (LitTy, LitTy)
typeOfUOp op = let dup a = (a, a) in
  case op of
    LOpShow -> (LTInt, LTStr)
    LOpNeg -> dup LTInt
    LOpNot -> dup LTBool

typeOfBOp :: LitBinOp -> (LitTy, LitTy)
typeOfBOp op = case op of
   LOpAdd -> dup LTInt
   LOpMinus -> dup LTInt
   LOpMul -> dup LTInt
   LOpConcat -> dup LTStr
   LOpG -> (LTInt, LTBool)
   LOpGE -> (LTInt, LTBool)
   LOpL -> (LTInt, LTBool)
   LOpLE -> (LTInt, LTBool)
   LOpEq -> (LTInt, LTBool)
   LOpNeq -> (LTInt, LTBool)
   LOpAnd -> dup LTBool
   LOpOr -> dup LTBool
  where dup a = (a, a)

instance Pretty LitTy where
  ppr _ ty = case ty of
    LTInt -> text "Int"
    LTStr -> text "String"
    LTAny -> text "Any"
    LTBool -> text "Bool"

instance Pretty LitVal where
  ppr _ val = case val of
    LVInt i -> int i
    LVStr s -> dquotes $ text (concatMap escapeString s)
    LVBool b -> text (show b)

instance Pretty LitUnaryOp where
  ppr _ op = case op of
    LOpShow -> char '@'
    LOpNeg -> text negSign
    LOpNot -> char '!'

instance Pretty LitBinOp where
  ppr _ op = case op of
    LOpAdd -> char '+'
    LOpMinus -> char '-'
    LOpMul -> text "**"
    LOpConcat -> text "++"
    LOpG -> text ">"
    LOpGE -> text ">="
    LOpL -> text ">"
    LOpLE -> text ">="
    LOpEq -> text "=="
    LOpNeq -> text "/="
    LOpAnd -> text "&&"
    LOpOr -> text "||"

escapeGeneral :: Char -> String
escapeGeneral '\b' = "\\b"
escapeGeneral '\t' = "\\t"
escapeGeneral '\n' = "\\n"
escapeGeneral '\f' = "\\f"
escapeGeneral '\r' = "\\r"
escapeGeneral '\\' = "\\\\"
escapeGeneral c | c >= ' ' && c < '\DEL' = [c]
                | otherwise =  printf "\\u%04x" (fromEnum c)

escapeChar :: Char -> String
escapeChar '\'' = "\\'"
escapeChar c = escapeGeneral c

escapeString :: Char -> String
escapeString '"' = "\\\""
escapeString c | c <= '\xFFFF' = escapeGeneral c
               | otherwise = escapeGeneral lead ++ escapeGeneral trail
                   where c' = fromEnum c - 0x010000
                         lead = toEnum $ 0xD800 + c' `div` 0x0400
                         trail = toEnum $ 0xDC00 + c' `mod` 0x0400
