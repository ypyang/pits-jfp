--> castup^2 [PTree Z] (\(_PTree : *) (_Empty : _PTree) (_Fork : Int -> PTree (S Z) -> PTree (S Z) -> _PTree) . _Fork 1 (Empty (S Z)) (Empty (S Z)))
{-

A labeled binary tree that keeps track its depth statically

-}

data Nat = Z | S Nat; 
data PTree (n : Nat) = Empty | Fork Int (PTree (S n)) (PTree (S n)); 
Fork Z 1 (Empty (S Z)) (Empty (S Z))
