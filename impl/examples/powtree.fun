--> "[42, 43]"
{-

A power tree, also define some help functions on lists, such as `foldr`,
`append`, `concatMap`, `head`, `tail`. The `toList` function transform
a power tree to a list

-}

data List a = Nil | Cons a (List a);
data PairT a = P a a;
data Pow a = Zero a | Succ (Pow (PairT a));

defrec foldr : (a : *) -> (b : *) -> (a -> b -> b) -> b -> List a -> b = 
\a : * . \b : * . \f : a -> b -> b . \y : b . \l : List a . 
  case l of 
    Nil => y | 
    Cons x xs => f x (foldr a b f y xs) ;

defrec append : (a : *) -> List a -> List a -> List a = 
\a : * . \l : List a . \m : List a . 
  case l of 
    Nil => m |
    Cons x xs => Cons a x (append a xs m) ;

def concatMap = 
\a : * . \b : * . \f : a -> List b . 
  foldr a (List b) (\x : a . \y : List b . append b (f x) y) (Nil b) ;

defrec toList : (a : *) -> Pow a -> List a = 
\a : * . \t : Pow a . 
  case t of 
    Zero x => Cons a x (Nil a) |
    Succ c => concatMap (PairT a) a 
              (\ x : PairT a . case x of 
                 P m n => Cons a m (Cons a n (Nil a))) (toList (PairT a) c) ;

defrec showListBody : (e : List Int) -> String =
\(e : List Int) .
    case e of
        Nil => "" |
        Cons x xs => ", " ++ @(x) ++ showListBody xs
;

def showList (e : List Int) : String = 
    case e of
        Nil => "[]" |
        Cons x xs => "[" ++ @(x) ++ (case xs of
            Nil => "" |
            Cons y ys => ", " ++ @(y) ++ showListBody ys
        :: String) ++ "]"
;

def test = Succ Int (Zero (PairT Int) (P Int 42 43)) ;
showList (toList Int test)
