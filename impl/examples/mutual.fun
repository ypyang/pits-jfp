--> True
defrec even' (odd : Int -> Bool) (n : Int) : Bool = 
    if n == 0 then True else odd (n - 1);
defrec odd' (even : Int -> Bool) (n : Int) : Bool = 
    if n == 0 then False else even (n - 1);
defrec even (n : Int) : Bool = even' (odd' even) n;
defrec odd (n : Int) : Bool = odd' (even' odd) n;
even 10