--> 1

-- Existential types
def Ex (P : * -> *) = (z : *) -> ((x : *) -> P x -> z) -> z;
def pack (P : * -> *) (e1 : *) (e2 : P e1) : Ex P =
  castup (\z : *. \f : (x : *) -> P x -> z. f e1 e2);
def unpack (P : * -> *) (C : *) (e : Ex P) (f : (x : *) -> P x -> C) =
  (castdn e :: (z : *) -> ((x : *) -> P x -> z) -> z) C f;

-- Pairs
data Pair (A : *) (B : *) = MkPair {fst : A, snd : B};

-- Object type operator
def Obj = \I:* -> *. Ex (\X : *. Pair X (X -> I X));

-- Cell interface
data Cell (X : *) = MkCell {get: Int, set: Int -> X, bump: X};

-- Instance of a cell object
data Var   = MkVar {getVar : Int};
def CellT  = \X:*. Pair X (X -> Cell X);
def pair   = MkPair Var (Var -> Cell Var)
                     (MkVar 0) 
                     (\s:Var. MkCell Var 
                                (getVar s)              -- get
                                (\n:Int. MkVar n)       -- set
                                (MkVar (getVar s + 1))  -- bump
                     );
def p      = pack  CellT Var (castup pair);
def c : Obj Cell = castup p;

-- Methods
def getM (o: Obj Cell) = 
      unpack CellT Int (castdn o) (\(X:*) (p: CellT X). 
        let s = fst X (X -> Cell X) (castdn p) in
        let m = snd X (X -> Cell X) (castdn p) in
        get X (m s));

def bumpM (o: Obj Cell) = 
      unpack CellT (Obj Cell) (castdn o) (\(X:*) (p: CellT X). 
        let s = fst X (X -> Cell X) (castdn p) in
        let m = snd X (X -> Cell X) (castdn p) in
        let s' = bump X (m s) in
        let p' = MkPair X (X -> Cell X) s' m in
        (castup (pack CellT X (castup p'))) :: Obj Cell);


getM (bumpM c)
