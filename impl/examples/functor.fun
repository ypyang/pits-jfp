--> castup^2 [Functor Maybe] (\(_Functor : *) (_Func : ((a : *) -> (b : *) -> (a -> b) -> Maybe a -> Maybe b) -> _Functor) . _Func (\(a : *) (b : *) (f : a -> b) (x : Maybe a) . (castdn^2 x) (Maybe b) (Nothing b) (\z : a . Just b (f z))))
{-

A functor record, also define `Maybe` datatype and its functor instance

-}

data Maybe (a : *) = Nothing | Just a;


data Functor (f : * -> *) =
  Func {fmap : (a : *) -> (b : *) -> (a -> b) -> f a -> f b};



def maybeInst =
  Func Maybe (\ a : * . \ b : * . \f : a -> b . \ x : Maybe a . 
    case x of
      Nothing => Nothing b
    | Just z => Just b (f z));


maybeInst
