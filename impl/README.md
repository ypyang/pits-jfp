# Implementation of PITS

## Introduction

This is an implementation of PITS and a simple functional language
**Fun** which is based on a specific "type-in-type" call-by-name PITS.
Both weak and full cast operators are supported in Fun:
`castup`/`castdn` for weak casts and `castup!`/`castdn!` for full
casts. **Fun** supports algebraic data types and pattern matching
using *weak* casts by *Scott encoding* (see Section 3.1). This
implementation contains a _REPL_ (Read-Eval-Print-Loop) that
interprets PITS, as well as a _compiler_ that compiles PITS to
JavaScript and Haskell.

## Syntactic difference from the paper

For convenience, we implement *simple* bidirectional
type checking to reduce annotations in the REPL. The type checker will complain
in case required annotations are missing. The syntax for cast operators
is slightly different from the one shown in the paper:

In paper | In REPL
---------|--------
`castup [A] e` | `(castup e :: A)`
`castdn e` | `(castdn e :: A)`

Notice that in some cases `castdn` also needs an annotation.

## Examples

See the [examples directory](examples/).
An [online compiler](https://fun-lang.github.io/)
is available for running examples in browsers.

Name                                   |  Description
-------------------------------------- | -------------
[polyList.fun](examples/polyList.fun)  | Polymorphic list (3.1)
[functor.fun ](examples/functor.fun )  | Higher-kinded Types (3.2-1)
[ptree.fun   ](examples/ptree.fun   )  | Datatype Promotion (3.2-2)
[hoas.fun    ](examples/hoas.fun    )  | HOAS (3.2-3)
[vector.fun  ](examples/vector.fun  )  | Leibniz Equality, Kind Polymorphism and Vectors (3.3)
[object.fun  ](examples/object.fun  )  | Object Encodings (3.4)
[kindPoly.fun](examples/kindPoly.fun)  | Kind polymorphism
[powtree.fun ](examples/powtree.fun )  | Power tree
[fact.fun    ](examples/fact.fun    )  | Factorial function
[fixpoint.fun](examples/fixpoint.fun)  | Fixpoints of functors
[mutual.fun  ](examples/mutual.fun  )  | Mutual recursion
[DepSum.fun  ](examples/DepSum.fun  )  | Weak dependent sums

## Build and run

You need to install
[*stack*](https://github.com/commercialhaskell/stack) into PATH and
run the following commands:

```bash
make
```

The REPL will run if built successfully. Run `make install` to install
the binary `fun` into `~/.local/bin`.

All examples shown above can be tested by:

```bash
make test
```

## REPL

Type `make` or `fun` (if installed into PATH) **with no arguments** to
invoke the REPL.

Use `tab` for command completion. Type `:?` to show full help. Some
useful commands are:

Command         | Description
-------------   | -------------
`<expr>`        | Evaluate the expression
`:t <expr>`     | Infer the type of expression
`:trans <expr>` | Dump translation of expression
`:l <file>`     | Load and execute the file
`:q`            | Quit the REPL
`:?`            | Show help

## Compiler

Run with arguments to specify the file to compile into JavaScript (or
Haskell), for example

```bash
fun examples/vector.fun      # add "-s" for Haskell
```

If [Node.js](https://nodejs.org/en/) is installed, use `-r` option to
directly run the program after compilation:

```bash
fun -r examples/vector.fun   # add "-s" for Haskell
```

To test all examples by Node.js, run

```bash
cd examples
make
```

Type `fun --help` for more information about the compiler.
 
## Quick Reference

A program consists of declarations separated by semicolon `;` ending
with a *single* expression. Like Haskell, a line comment starts with
`--` and a comment block is wrapped by `{-` and `-}`. Declarations
include datatypes and `def/defrec` bindings:

```
program := decl ; ... ; decl ; expr
decl    := data ... | def ... | defrec ...
```

Here is a short list of supported language features:

+ Literals: 
    * `Int` for non-negative integers
    * `String` for strings
    * `+` for plus, `-` for minus, `**` for multiply (not `*` which is
     reserved for "type-of-type")
    * `++` for string concatenation, `@(e)` for conversion an integer
     to a string
+ Built-in boolean, logical operators and if-else-then clause
+ Annotation: `1 :: Int`
+ Kind expression: `*`, `* -> *`
+ Lambda expression: `\x : Int . x + 1` (needs a __space__ after `.`)
+ Function type: `Int -> Int`
+ Dependent product type: `(f : Int -> *) -> (x : Int) -> f x`
+ Recursive type: `/x . Int -> x :: *`
+ Weak casts: `castdn (castup 3 :: (\x : * . x) Int) :: Int`
+ Full casts: `\f : Int -> *. \x : f (1 + 1) . (castdn! x :: f 2)`
+ Datatype: `data List a = Nil | Cons a (List a);` (needs a __semicolon__ at the end)
+ Record: `data Person = P {age : Int, address : String};` (needs a __semicolon__ at the end)
+ Case expression: `case (Nil Int) of Nil => -1 | Cons x xs => x`
+ Let/letrec local binding: `let id (x : *) = x in id Int`
+ Def/defrec global binding: `defrec f (x : Int) : Int = f x;` (needs a __semicolon__ at the end)
