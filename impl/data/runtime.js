Function.prototype.app = function () {
    var self = this;
    var args = Array.prototype.slice.call(arguments);
    return function () {
        return self.apply(null, args.concat(Array.prototype.slice.call(arguments)));
    };
};

function $r(v) {
    this.raw = v;
}

function $i(f, a) {
    this.par = f;
    this.args = Array.prototype.slice.call(a);
}

function $f(e) {
    while (typeof e === "function") {
        e = e(); 
    }
    if (e instanceof $r) {
        e = e.raw;
    } else if (e === null) {
        e = "()";
    } else if (e instanceof $i) {
        e = e.par.toString();
    } else if (typeof e === "string") {
        e = JSON.stringify(e);
    }
    return e;
}

function $fi(e) {
    while (typeof e === "function") {
        e = e(); 
    }
    if (e instanceof $i) {
        return function () {
            return e.par.apply(null, e.args.concat(Array.prototype.slice.call(arguments)));
        }
    } else {
        return e;
    }
}

function $p(t, es, n) {
    if (es.length > n) {
        var args = Array.prototype.slice.call(es, n);
        return t.app.apply(t, args);
    } else {
        return t;
    }
}

var $o = {
    unary: function (op, f, e) {
        if (typeof e === "function") {
            return op.app(e());
        } else {
            return f(e);
        }
    },
    binary: function (op, f, e1, e2) {
        if (typeof e1 === "function") {
            return op.app(e1(), e2);
        } else if (typeof e2 === "function") {
            return op.app(e1, e2());
        } else {
            return f(e1, e2);
        }
    },
    show: function (x) {
        return $o.unary($o.show, (function (e) {
            if (e instanceof $r) {
                e = e.raw;
            }
            var s = e;
            if (e === null) {
                s = "()";
            } else if (typeof e === "function") {
                s = e.toString();
            }
            return JSON.stringify(s);
        }), x);
    },
    neg: function (x) {
        return $o.unary($o.neg, (function (x) {
            return -x;
        }), x);
    },
    plus: function (x, y) {
        return $o.binary($o.plus, (function (x, y) {
            return x + y || 0;
        }), x, y);
    },
    minus: function (x, y) {
        return $o.binary($o.minus, (function (x, y) {
            return x - y;
        }), x, y);
    },
    mul: function (x, y) {
        return $o.binary($o.mul, (function (x, y) {
            return x * y;
        }), x, y);
    },
    concat: function (x, y) {
        return $o.binary($o.concat, (function (x, y) {
            return x + y || "";
        }), x, y);
    },
    ifthenelse: function (x, y, z) {
        if (typeof x === "function") {
            return $o.ifthenelse.app(x(), y, z);
        } else if (x === true) {
            return y;
        } else {
            return z;
        }
    },
    not: function (x) {
        return $o.unary($o.not, (function (x) {
            return x !== true;
        }), x);
    },
    grt: function (x, y) {
        return $o.binary($o.grt, (function (x, y) {
            return x > y;
        }), x, y);
    },
    grteq: function (x, y) {
        return $o.binary($o.grteq, (function (x, y) {
            return x >= y;
        }), x, y);
    },
    less: function (x, y) {
        return $o.binary($o.less, (function (x, y) {
            return x < y;
        }), x, y);
    },
    lesseq: function (x, y) {
        return $o.binary($o.lesseq, (function (x, y) {
            return x <= y;
        }), x, y);
    },
    eq: function (x, y) {
        return $o.binary($o.eq, (function (x, y) {
            return x === y;
        }), x, y);
    },
    neq: function (x, y) {
        return $o.binary($o.neq, (function (x, y) {
            return x !== y;
        }), x, y);
    },
    and: function (x, y) {
        return $o.binary($o.and, (function (x, y) {
            return x && y;
        }), x, y);
    },
    or: function (x, y) {
        return $o.binary($o.or, (function (x, y) {
            return x || y;
        }), x, y);
    },
    fix: function (f) {
        var g = f($o.fix.app(f));
        return g.app.apply(g, Array.prototype.slice.call(arguments, 1));
    }
};
