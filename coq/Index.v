(** This file contains main results of the paper **)

Require Import LibLN PTS_Spec.
Require LamI_ott LamI_Proofs LamI_Dec.
Require LamI_CBV_ott LamI_CBV_Proofs LamI_CBV_Dec.
Require LamI_Full_ott LamI_Full_Proofs LamI_Full_Dec.
Require PTSMu_Sound PTSMu_Single.

(* Theorems in Section 4 *)

Module Type CallByName.
  
(* **** Syntax and Relations **** *)

  (* Term *)
  Parameter trm : Set.

  (* Term is closed *)
  Parameter lc_trm : trm -> Prop.
  Notation "|~ e" := (lc_trm e) (at level 67).

  (* Context *)
  Definition env := LibEnv.env trm.

  (* Substitution *)
  Parameter subst : var -> trm -> trm -> trm.
  Notation "t [ z := u ]" := (subst z u t) (at level 68).
  Notation "t [[ z := u ]]" := (map (subst z u) t) (at level 68).

  (* Operational Semantics *)
  Parameter reduct : trm -> trm -> Prop.
  Parameter value : trm -> Prop.
  Notation "t1 --> t2" := (reduct t1 t2) (at level 67).

  (* Typing *)
  Parameter typing : env -> trm -> trm -> Prop.
  Notation "G |= e ~: A" := (typing G e A) (at level 67).
    (* In paper: G |- e : A *)

  (* Context Well-formedness *)
  Parameter wf : env -> Prop.
  Notation "|= G" := (wf G) (at level 67).
    (* In paper: |- G *)

  (* Decidable *)
  Definition decidable (P : Prop) := (P \/ ~ P).

(* **** Lemmas **** *)
  
  (* Lemma 4.1: Determinacy of One-step Reduction *)
  Parameter lem_determ_reduct : forall e e1 e2,
    e --> e1 -> e --> e2 -> e1 = e2.

  (* Lemma 4.2: Uniqueness of Typing *)
  Parameter lem_unique_typing : forall G e A B,
    G |= e ~: A -> G |= e ~: B -> A = B.

  (* Theorem 4.1: Decidability of Type Checking *)
  Parameter lem_decidable_typing : forall G e,
    |~ e -> |= G -> decidable (exists A, G |= e ~: A).

  (* Theorem 4.2: Subject Reduction *)
  Parameter lem_subject_reduction : forall G e e' A,
    G |= e ~: A -> e --> e' -> G |= e' ~: A.

  (* Theorem 4.3: Progress *)
  Parameter lem_progress : forall e A,
      empty |= e ~: A -> value e \/ exists e', e --> e'.

  (* Lemma 4.3: Substitution *)
  Parameter lem_substitution : forall B (G2:env) e2 (G1:env) x e1 A,
    G1 |= e2 ~: B ->
    G1 & x ~ B & G2 |= e1 ~: A ->
    G1 & (G2 [[x := e2]]) |= (e1 [x := e2]) ~: (A [x := e2]).

End CallByName.

(* Theorems in Section 5 *)

Module Type CallByValue.
  
(* **** Syntax and Relations **** *)

  (* Term *)
  Parameter trm : Set.

  (* Term is closed *)
  Parameter lc_trm : trm -> Prop.
  Notation "|~ e" := (lc_trm e) (at level 67).

  (* Context *)
  Definition env := LibEnv.env trm.

  (* Substitution *)
  Parameter subst : var -> trm -> trm -> trm.
  Notation "t [ z := u ]" := (subst z u t) (at level 68).
  Notation "t [[ z := u ]]" := (map (subst z u) t) (at level 68).

  (* Operational Semantics *)
  Parameter reduct : trm -> trm -> Prop.
  Parameter value : trm -> Prop.
  Notation "t1 --> t2" := (reduct t1 t2) (at level 67).

  (* Typing *)
  Parameter typing : env -> trm -> trm -> Prop.
  Notation "G |= e ~: A" := (typing G e A) (at level 67).
    (* In paper: G |-v e : A *)

  (* Context Well-formedness *)
  Parameter wf : env -> Prop.
  Notation "|= G" := (wf G) (at level 67).
    (* In paper: |-v G *)

  (* Decidable *)
  Definition decidable (P : Prop) := (P \/ ~ P).

(* **** Lemmas **** *)

  (* Lemma 5.1: Substitution *)
  Parameter lem_substitution : forall B (G2:env) v (G1:env) x e A,
    G1 |= v ~: B ->
    G1 & x ~ B & G2 |= e ~: A ->
    value v ->
    G1 & (G2 [[x := v]]) |= (e [x := v]) ~: (A [x := v]).

  (* Theorem 5.1: Subject Reduction *)
  Parameter lem_subject_reduction : forall G e e' A,
    G |= e ~: A -> e --> e' -> G |= e' ~: A.

  (* Theorem 5.2: Progress *)
  Parameter lem_progress : forall e A,
      empty |= e ~: A -> value e \/ exists e', e --> e'.

  (* Lemma 5.2: Determinacy of One-step Reduction *)
  Parameter lem_determ_reduct : forall e e1 e2,
    e --> e1 -> e --> e2 -> e1 = e2.

  (* Lemma 5.3: Uniqueness of Typing *)
  Parameter lem_unique_typing : forall G e A B,
    G |= e ~: A -> G |= e ~: B -> A = B.

  (* Theorem 5.3: Decidability of Type Checking *)
  Parameter lem_decidable_typing : forall G e,
    |~ e -> |= G -> decidable (exists A, G |= e ~: A).

End CallByValue.

Module Type FullPITS.

(* **** Syntax and Relations **** *)

  (* Term *)
  Parameter trm : Set.

  (* Erased Term *)
  Parameter etrm : Set.

  (* Term is closed *)
  Parameter lc_trm : trm -> Prop.
  Notation "|~ e" := (lc_trm e) (at level 67).
  Parameter lc_etrm : etrm -> Prop.
  Notation "|~~ r" := (lc_etrm r) (at level 67).

  (* Context *)
  Definition env := LibEnv.env trm.

  (* Erased Context *)
  Definition eenv := LibEnv.env etrm.

  (* Substitution *)
  Parameter esubst : var -> etrm -> etrm -> etrm.
  Notation "t [ z := u ]" := (esubst z u t) (at level 68).
  Notation "t [[ z := u ]]" := (map (esubst z u) t) (at level 68).

  (* Typing *)
  Parameter typing : env -> trm -> trm -> Prop.
  Notation "G |= e ~: A" := (typing G e A) (at level 67).
    (* In paper: G |-f e : A *)
  (* Context Well-formedness *)
  Parameter wf : env -> Prop.
  Notation "|= G" := (wf G) (at level 67).
    (* In paper: |-f G *)

  (* Typing of PTSMu *)
  Parameter etyping : eenv -> etrm -> etrm -> Prop.
  Notation "D |~ r ~: R" := (etyping D r R) (at level 67).
    (* In paper: D |- r : R *)

  (* Typing of PTSStep *)
  Parameter styping : eenv -> etrm -> etrm -> Prop.
  Notation "D |== r ~: R" := (styping D r R) (at level 67).
    (* In paper: D |= r : R *)

  (* Operational Semantics of PTSMu *)
  Parameter reduct : etrm -> etrm -> Prop.
  Parameter value : etrm -> Prop.
  Notation "r1 --> r2" := (reduct r1 r2) (at level 67).
  Parameter dpara : etrm -> etrm -> Prop.
  Notation "r1 -p> r2" := (dpara r1 r2) (at level 67).
  Parameter dpara_iter : etrm -> etrm -> Prop.
  Notation "r1 -p>> r2" := (dpara_iter r1 r2) (at level 67).
  Parameter beta : etrm -> etrm -> Prop.
  Notation "r1 ~~> r2" := (beta r1 r2) (at level 67).
  Parameter beta_star : etrm -> etrm -> Prop.
  Notation "r1 ~~*> r2" := (beta_star r1 r2) (at level 67).

  (* Erasure *)
  Parameter erasure : trm -> etrm.
  Notation "[# e #]" := (erasure e) (at level 2).
    (* In paper: |e| *)
  Notation "{# G #}" := (map erasure G) (at level 2).
    (* In paper: |G| *)

  (* Decidable *)
  Definition edecidable (P : Prop) := (P \/ ~ P).
  Definition decidable (P : Prop) := (P \/ ~ P).

(* **** Theorems **** *)
  
  (* Lemma 6.1: Substitution of Erased System *)
  Parameter lem_substitution_erased : forall R' (D2:eenv) r2 (D1:eenv) x r1 R,
    D1 |~ r2 ~: R' ->
    D1 & x ~ R' & D2 |~ r1 ~: R ->
    D1 & (D2 [[x := r2]]) |~ (r1 [x := r2]) ~: (R [x := r2]).

  (* Theorem 6.1: Progress of PTSMu *)
  Parameter lem_progress : forall r R,
    empty |~ r ~: R -> value r \/ exists r', r --> r'.

  (* Lemma 6.2-1: Subject Reduction for Single-step Beta *)
  Parameter lem_subject_reduction_beta : forall D r r' R,
    r ~~> r' -> D |~ r ~: R -> D |~ r' ~: R.

  (* Lemma 6.2-2: Subject Reduction for Multi-step Beta *)
  Parameter lem_subject_reduction_beta_star : forall D r r' R,
    r ~~*> r' -> D |~ r ~: R -> D |~ r' ~: R.

  (* Lemma 6.3: Equivalence of Parallel Reduction *)
  Parameter lem_para_iter_is_beta_star : forall r1 r2,
    r1 ~~*> r2 <-> r1 -p>> r2.

  (* Theorem 6.2: Subject Reduction or Parellel Reduction *)
  Parameter lem_subject_reduction : forall D r r' R,
    r -p> r' -> D |~ r ~: R -> D |~ r' ~: R.

  (* Lemma 6.4: Soundness of Erasure *)
  Parameter lem_erasure_sound : forall G e A,
    G |= e ~: A -> {#G#} |~ [#e#] ~: [#A#].

  (* Lemma 6.5: Decidability of Parallel Reduction *)
  Parameter lem_dpara_decidable : forall r1 r2,
    |~~ r1 -> |~~ r2 ->
    edecidable (r1 -p> r2).

  (* Lemma 6.6: Uniqueness of Typing *)
  Parameter lem_unique_typing : forall G e A B,
    G |= e ~: A -> G |= e ~: B -> A = B.

  (* Theorem 6.3: Decidability of Type Checking *)
  Parameter lem_decidable_typing : forall G e,
    |~ e -> |= G -> decidable (exists A, G |= e ~: A).

  (* Lemma 6.7: Completeness of PITS to PTSStep *)
  Parameter lem_completeness : forall D r R,
    D |== r ~: R -> exists G e A,
    {#G#}=D /\ [#e#] = r /\ [#A#] = R /\
    G |= e ~: A.

  (* Lemma 6.8 and Theorem 6.4 are derived from *)
  (*   Corollary 2.9 in "Pure type system conversion is always typable" *)
  (*   Authors: Siles, Vincent, & Herbelin, Hugo *)
  (*   Published in 2012, Journal of functional programming, 22(2), 153–180. *)

End FullPITS.

(* ******* Verification ******* *)

(* "LamI" proves all lemmas in Section 4 *)

Module CBNImpl : CallByName.
  Module M := LamI_ott.
  Module P := LamI_Proofs.
  Module D := LamI_Dec.

  Definition trm := M.trm.
  Definition lc_trm := M.lc_trm.
  Definition env := M.env.
  Definition subst z u t := M.subst_trm u z t.
  Definition reduct := M.reduct.
  Definition value := M.value.
  Definition typing := M.typing.
  Definition wf := M.wf.
  Definition decidable := D.decidable.

  Definition lem_determ_reduct := P.reduct_determ.
  Definition lem_unique_typing := D.typing_unique.
  Definition lem_decidable_typing := D.typing_decidable.
  Definition lem_subject_reduction := P.typing_preserve.
  Definition lem_progress := P.typing_progress.
  Definition lem_substitution := P.typing_substitution.
End CBNImpl.

(* "LamI_CBV" proves all lemmas in Section 5 *)

Module CBVImpl : CallByValue.
  Module M := LamI_CBV_ott.
  Module P := LamI_CBV_Proofs.
  Module D := LamI_CBV_Dec.

  Definition trm := M.trm.
  Definition lc_trm := M.lc_trm.
  Definition env := M.env.
  Definition subst z u t := M.subst_trm u z t.
  Definition reduct := M.reduct.
  Definition value := M.value.
  Definition typing := M.typing.
  Definition wf := M.wf.
  Definition decidable := D.decidable.

  Definition lem_determ_reduct := P.reduct_determ.
  Definition lem_unique_typing := D.typing_unique.
  Definition lem_decidable_typing := D.typing_decidable.
  Definition lem_subject_reduction := P.typing_preserve.
  Definition lem_progress := P.typing_progress.
  Definition lem_substitution := P.typing_substitution.
End CBVImpl.

(* "LamI_Full" and "PTSMu" prove all lemmas in Section 6 *)

Module FullImpl : FullPITS.
  Module M := LamI_Full_ott.
  Module P := LamI_Full_Proofs.
  Module D := LamI_Full_Dec.
  Module E := PTSMu_ott.
  Module ED := PTSMu_Dec.
  Module EP := PTSMu_Sound.
  Module ES := PTSMu_Single.

  Definition trm := M.trm.
  Definition etrm := E.trm.
  Definition lc_trm := M.lc_trm.
  Definition lc_etrm := E.lc_trm.
  Definition env := M.env.
  Definition eenv := E.env.
  Definition esubst z u t := E.subst_trm u z t.
  Definition typing := M.typing.
  Definition wf := M.wf.
  Definition etyping := E.typing.
  Definition styping := E.styping.
  Definition reduct := E.reduct.
  Definition value := E.value.
  Definition dpara := E.dpara.
  Definition dpara_iter := (E.iter_ E.dpara).
  Definition beta := E.beta.
  Definition beta_star := (E.star_ E.beta).
  Definition erasure := M.erasure.
  Definition edecidable := ED.decidable.
  Definition decidable := D.decidable.

  Definition lem_substitution_erased := EP.typing_substitution.
  Definition lem_subject_reduction_beta := EP.subject_reduction_result.
  Definition lem_subject_reduction_beta_star := EP.subject_reduction_star_result.
  Definition lem_para_iter_is_beta_star := ED.beta_star_is_dpara_iter.
  Definition lem_subject_reduction := ED.subject_reduction_dpara.
  Definition lem_progress := EP.progress_era.
  Definition lem_erasure_sound := P.era_typing.
  Definition lem_dpara_decidable := ED.dpara_dec.
  Definition lem_unique_typing := D.typing_unique.
  Definition lem_decidable_typing := D.typing_decidable.
  Definition lem_completeness := ES.styping_completeness.
End FullImpl.

