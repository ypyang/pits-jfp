Set Implicit Arguments.
Require Import LibLN PTSMu_ott PTSMu_Infra PTSMu_CR.
Implicit Types x : var.

(* ********************************************************************** *)
(** ** Inversion Lemmas for Typing *)

Lemma typing_prod_inv : forall E U T P,
  typing E (trm_prod U T) P -> exists L s1 s2 s3,
     conv (trm_sort s3) P
  /\ typing E U (trm_sort s1)
  /\ forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T). 
  induction Typ; intros; subst; tryfalse.
  injection EQP1. intros. subst.
  exists* L s1 s2 s3.
  destruct~ (IHTyp1 eq_refl) as [L [s1 [s2 [s3 [EQi [TypU P]]]]]].
  lets Q: (@equiv_trans beta A).
  exists* L s1 s2 s3.
Qed.

Lemma typing_abs_inv : forall E V t P,
  typing E (trm_abs V t) P -> exists T L s1 s2,
     typing E V (trm_sort s1)
  /\ (forall x, x \notin L -> typing (E & x ~ V) (T ^ x) (trm_sort s2))
  /\ (conv (trm_prod V T) P)
  /\ (forall x, x \notin L -> typing (E & x ~ V) (t ^ x) (T ^ x)).
Proof.
  introv Typ. gen_eq u: (trm_abs V t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* B L s1 s2. splits*. apply* equiv_refl.
  apply_fresh* lc_trm_prod as y.
  lets: H1 y __. auto. auto.
  destruct (IHTyp1 eq_refl) as [T0 [L [s1 [s2 [TypE [TypT0 [Le P2]]]]]]].
  exists* T0 L s1 s2.
Qed.

Lemma typing_prod_type_inv : forall E U T s3,
  typing E (trm_prod U T) (trm_sort s3) ->
  exists L s2, forall x, x \notin L -> 
      typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. 
  destruct (typing_prod_inv Typ) as [L [s1 [s2 [s3' [Le [TypU TypT]]]]]].
  exists* (L \u dom E) s2.
Qed.

(* ********************************************************************** *)
(** Typing preserved by Weakening *)

Lemma typing_weaken : forall G E F t T,
  typing (E & G) t T ->
  wf (E & F & G) -> 
  typing (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var *)
  apply* t_var. apply* binds_weaken.
  (* case: trm_abs *)
  apply_fresh* (@t_abs) as y.
  apply_ih_bind* H0. apply_ih_bind* H2.
  (* case: trm_prod *)
  apply_fresh* (@t_prod) as y. apply_ih_bind* H0.
  (* case: trm_mu *)
  apply_fresh* (@t_mu) as y. apply_ih_bind* H0.
Qed.

(* ********************************************************************** *)
(** Typing preserved by Substitution *)

Lemma t_conv' : forall (A : trm) (G:env) (e B:trm) (s:sort),
     typing G e A ->
     typing G B (trm_sort s) ->
      ((beta equiv) ( A ) ( B ))  ->
      typing G e B.
Proof. intros. eauto. Qed.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  typing E v V ->
  typing (E & x ~ V & F) t T ->
  typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) => 
      forall F, G = E & x ~ V & F -> 
      wf (E & (map (subst x v) F))); 
   intros; subst; simpls subst. 
  (* case: trm_type *)
  autos*.
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ t_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh* (@t_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
  (* case: trm_app *)
  rewrite* subst_open.
  (* case: trm_prod *)
  apply_fresh* (@t_prod) as y.
   cross; auto. apply_ih_map_bind* H0. 
  (* case: trm_mu *)
  apply_fresh* (@t_mu) as y.
  cross; auto. apply_ih_map_bind* H0.
  (* case: sub *)
  apply* (@t_conv' ([x ~> v]A)). apply* conv_red_out.
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
  (* case: conclusion *)
  auto.
Qed.

(* ********************************************************************** *)
(** Types are Themselves Well-typed *)

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  exists s, typing E U (trm_sort s).
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     exists s. apply_empty* typing_weaken.
    destruct (wf_push_inv W).
     destruct~ IHE as [s ?]. exists s.
     apply_empty* typing_weaken.
Qed.

Lemma typing_wf_from_typing : forall E t T,
  typing E t T ->
  exists s, T = trm_sort s \/ 
            typing E T (trm_sort s).
Proof.
  induction 1; eauto 4.
  destruct* (typing_wf_from_context H0).
  destruct IHtyping1 as [s [? | Typ]].
  false.
  destruct (typing_prod_inv Typ) as [L [s1 [s2 [s3' [Le [TypU TypT]]]]]].
   pick_fresh x. rewrite~ (@subst_intro x).
   exists s2. unsimpl ([x ~> e2](trm_sort s2)).
   right. apply_empty* (@typing_substitution A).
Qed.

(* ********************************************************************** *)
(** Typing preserved by Subtyping in Environment *)

Inductive env_less : env -> env -> Prop :=
  | env_less_head : forall E U U' x, 
      conv U U' -> 
      env_less (E & x ~ U) (E & x ~ U')
  | env_less_tail : forall E E' x U,
      lc_trm U -> env_less E E' -> env_less (E & x ~ U) (E' & x ~ U).

Hint Constructors env_less.

Lemma env_less_binds : forall x U E E',
  env_less E' E -> wf E -> wf E' -> binds x U E -> 
     binds x U E' 
  \/ exists U' s, 
      binds x U' E' /\ conv U' U /\ typing E' U (trm_sort s).
Proof.
  introv Le. induction Le; intros WfE WfE' Has.
  destruct (binds_push_inv Has) as [[? ?]|[? ?]]. 
    subst. right. inversions WfE. false (empty_push_inv H1).
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     exists U0 s. splits~. apply_empty* typing_weaken.  
    left. apply~ binds_push_neq.
  destruct (binds_push_inv Has) as [[? ?]|[? ?]].
    subst. left. apply~ binds_push_eq.
    inversions keep WfE. false (empty_push_inv H3).
     inversions WfE'. false (empty_push_inv H6).
     destruct (eq_push_inv H5) as [? [? ?]]. subst.
     destruct (wf_push_inv WfE).
     destruct IHLe as [|[U' [s' [P1 [P2 P3]]]]]; auto. 
      right. exists U' s'. splits~. apply_empty* typing_weaken.
Qed. 

Lemma typing_sub_env : forall E E' t T,
  typing E t T -> 
  env_less E' E -> 
  wf E' -> 
  typing E' t T.
Proof.
 introv Typ. gen E'. induction Typ; intros E' C W; eauto.
 destruct (env_less_binds C H W H0) as [B |[U' [B [Le Typ]]]].
   apply* t_var.
   apply* (@t_conv' U').
  apply_fresh* (@t_abs) as y.
  apply_fresh* (@t_prod) as y.
  apply_fresh* (@t_mu) as y.
Qed.

(* ********************************************************************** *)
(** Subject Reduction - Induction *)

Lemma subject_reduction_ind : forall E t t' T,
  typing E t T -> beta t t' -> typing E t' T.
Proof.
  introv Typ. gen t'.
  induction Typ; introv Red;
   try solve [ eauto 3 using t_conv' ]; inversions Red.

  (* case: trm_abs 1 *)
  apply* (@t_conv' (trm_prod A' B)).
    apply_fresh* (@t_abs) as y.
     apply (@typing_sub_env (G & y ~ A)); eauto 7.
     apply (@typing_sub_env (G & y ~ A)); eauto 7.
    apply~ equiv_sym. apply~ equiv_step.
    apply~ beta_prod1. apply_fresh* lc_trm_prod as y.
    lets: H1 y __. auto. auto.

  (* case: trm_abs 2 *)
  apply_fresh* (@t_abs) as y.

  (* case: trm_app - beta reduction *)
  destruct (typing_abs_inv Typ1) as [T' [L1 [s1 [s2 [Typt1 [TypT' [LeP Typt2]]]]]]].
  destruct (conv_prod_prod_inv LeP) as [C [L3 LeT]].
  destruct (typing_wf_from_typing Typ1) as [s [? | TypUT]]. false.
  destruct (typing_prod_type_inv TypUT) as [L4 [s3 TypT]].
  pick_fresh x.
   rewrite* (@subst_intro x e0).
   rewrite* (@subst_intro x B).
  lets: TypT x __. auto.
  apply_empty* (@typing_substitution A).
    apply~ (@t_conv' (T' ^ x)).
     apply* (@typing_sub_env (G & x ~ A0)).
     apply* LeT.

  (* case: trm_app 1 *)
  autos*.

  (* case: trm_app 2 *)
  destruct (typing_wf_from_typing Typ1) as [s [? | TypUT]]. false.
  destruct (typing_prod_type_inv TypUT) as [L [s' TypT]].
  apply (@t_conv' (B ^^ e2')) with (s := s').
    apply* t_app.
    pick_fresh x. rewrite* (@subst_intro x B).
     unsimpl (subst x e2 (trm_sort s')).
     apply_empty* (@typing_substitution A).
    apply* conv_from_open_beta.

  (* case: trm_prod 1 *)
  apply_fresh* (@t_prod) as y.
   apply (@typing_sub_env (G & y ~ A)); eauto 7. 

  (* case: trm_prod 2 *)
  apply_fresh* (@t_prod) as y.

  (* case: trm_mu - reduction *)
  pick_fresh x.
  assert ([x ~> trm_mu A e] A = A).
  apply* subst_fresh.
  rewrite <- H1 at 2.
  rewrite* (@subst_intro x e).
  apply_empty* (@typing_substitution ([x ~> trm_mu A e] A)).
  rewrite* H1. rewrite* H1.

  (* case: trm_mu 1 *)
  forwards~ Typt1'T: (IHTyp A').
  apply* (@t_conv' A').
  apply_fresh* (@t_mu) as y.
  forwards~ K: (H y).
  apply* (@typing_sub_env (G & y ~ A)).
  apply* (@t_conv' A). instantiate (1 := s).
  apply_empty* typing_weaken.

  (* case: trm_mu 2 *)
  apply_fresh* (@t_mu) as y.
Qed.

(** Subject relation (proved for beta and beta star) *)

Definition subject_reduction (R : relation) :=
  forall E t t' T,
  R t t' -> 
  typing E t T -> 
  typing E t' T.

(* ********************************************************************** *)
(** Subject Reduction - Beta preserves typing  *)

Lemma subject_reduction_result : subject_reduction beta.
Proof.
  introv Red Typ. apply* subject_reduction_ind.
Qed.

(** Subject Reduction - Beta Star preserves typing  *)

Lemma subject_reduction_star_result : 
  subject_reduction (beta star).
Proof.
  introv H. induction* H. apply* subject_reduction_result.
Qed.

(** Subject Reduction - Parallel Reduction preserves typing  *)

Lemma subject_reduction_para_iter_result : 
  subject_reduction (para iter).
Proof.
  introv H. induction* H.
  apply* subject_reduction_star_result.
  apply* para_iter_to_beta_star.
Qed.

Lemma subject_reduction_para_result :
  subject_reduction para.
Proof.
  introv H. 
  pose (iter_step para t t' H).
  apply* subject_reduction_para_iter_result.
Qed.

(* ********************************************************************** *)
(** Progress of weak-head reduction *)

Lemma typing_sort_inv : forall G A s,
  typing G (trm_sort s) A ->
  exists s', conv A (trm_sort s').
Proof.
  introv H. inductions H; eauto 3.
  exists* s2.
  destruct~ (IHtyping1 s) as [s' EQ].
  exists* s'.
Qed.

Lemma progress_era : forall t T, 
  typing empty t T ->
     value t 
  \/ exists t', t --> t'.
Proof.
  introv Typ. lets Typ': Typ. inductions Typ.
  left*.
  false* binds_empty_inv. 
  left*.
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1. exists* (e ^^ e2).
      false. destruct (typing_sort_inv H) as [s' ?].
      apply* conv_type_prod_inv.
      exists* (e ^^ e2).
      destruct~ (typing_prod_inv H) as [L [s1 [s2 [s3' [Le [TypU TypT]]]]]].
      false. lets P: (equiv_trans beta _ _ _ Le H1).
      apply* (conv_type_prod_inv P).
      exists* (trm_app t1' e2).
  left*.
  right. exists* (e ^^ trm_mu A e).
  autos*.
Qed.

