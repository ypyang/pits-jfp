Set Implicit Arguments.
Require Import LibLN PTSMu_ott PTSMu_Infra PTSMu_CR PTSMu_Sound PTSMu_Dec.
Implicit Types x : var.

(* ********************************************************************** *)
(** Typing preserved by Weakening *)

Lemma styping_weaken : forall G E F t T,
  styping (E & G) t T ->
  swf (E & F & G) -> 
  styping (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var *)
  apply* ts_var. apply* binds_weaken.
  (* case: trm_abs *)
  apply_fresh* (@ts_abs) as y.
  apply_ih_bind* H0. apply_ih_bind* H2.
  (* case: trm_prod *)
  apply_fresh* (@ts_prod) as y. apply_ih_bind* H0.
  (* case: trm_mu *)
  apply_fresh* (@ts_mu) as y. apply_ih_bind* H0.
Qed.

(* ********************************************************************** *)
(** Typing preserved by Substitution *)

Lemma styping_substitution : forall V (F:env) v (E:env) x t T,
  styping E v V ->
  styping (E & x ~ V & F) t T ->
  styping (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply styping_induct with
   (P := fun (G:env) t T (Typt : styping G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      styping (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:swf G) => 
      forall F, G = E & x ~ V & F -> 
      swf (E & (map (subst x v) F))); 
   intros; subst; simpls subst. 
  (* case: trm_type *)
  autos*.
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* styping_weaken.
    apply~ ts_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh* (@ts_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
  (* case: trm_app *)
  rewrite* subst_open.
  (* case: trm_prod *)
  apply_fresh* (@ts_prod) as y.
   cross; auto. apply_ih_map_bind* H0. 
  (* case: trm_mu *)
  apply_fresh* (@ts_mu) as y.
  cross; auto. apply_ih_map_bind* H0.
  (* case: sub *)
  apply* (@ts_castup). apply* beta_red_out.
  apply* (@ts_castdn). apply* beta_red_out.
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@swf_cons). 
  (* case: conclusion *)
  auto.
Qed.

(* ********************************************************************** *)
(** Lemmas of typing renaming *)

Lemma styping_rename_eq : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  styping (E & (x ~ T1)) (e ^ x) T2 ->
  styping (E & (y ~ T1)) (e ^ y) T2.
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  tests EQ: (x = y).
  subst; eauto.
  assert (swf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  assert ([x ~> trm_var_f y] T2 = T2).
  apply* subst_fresh.
  rewrite <- H0.
  apply_empty* (@styping_substitution T1).
  apply~ (@styping_weaken).
  apply swf_cons with (s := s); auto.
  apply_empty~ styping_weaken.
  apply swf_cons with (s := s); auto.
Qed.

Lemma styping_rename : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  styping (E & (x ~ T1)) (e ^ x) (T2 ^ x) ->
  styping (E & (y ~ T1)) (e ^ y) (T2 ^ y).
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  tests EQ: (x = y).
  subst; eauto.
  assert (swf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  rewrite* (@subst_intro x T2).
  apply_empty* (@styping_substitution T1).
  apply~ (@styping_weaken).
  apply swf_cons with (s := s); auto.
  apply_empty~ styping_weaken.
  apply swf_cons with (s := s); auto.
Qed.    

(* ********************************************************************** *)
(** ** Equivalence to LamI *)

Require LamI_Full_ott LamI_Full_Infra.
Module F := LamI_Full_ott.
Module FI := LamI_Full_Infra.
Notation "t @ x" := (F.open_trm_wrt_trm t (F.trm_var_f x)) (at level 67).
Notation "t @@ u" := (F.open_trm_wrt_trm t u) (at level 67).

Require LamI_Full_Proofs.
Module FS := LamI_Full_Proofs.

Lemma binds_map_exists : forall A B (f : A -> B) x b0 (E:LibEnv.env A),
  binds x b0 (map f E) ->
  exists a, f a = b0 /\ binds x a E.
Proof.
  intros. induction E using env_ind.
  rewrite map_empty in H.
  false* binds_empty_inv.
  autorewrite with rew_env_map in H.
  destruct (binds_push_inv H) as [[? ?]|[? ?]]. 
  subst~. exists* v.
  destruct (IHE H1) as [a1 [? ?]].
  subst. exists* a1.
Qed.

Lemma close_erase_rec : forall k x a,
  close_var_rec k x (F.erasure a) = F.erasure (FI.close_var_rec k x a).
Proof.
  intros. gen k x. induction a; eauto; simpls; intros; fequal~.
  case_if~.
Qed.  
  
Lemma close_erase : forall x a,
  close_var x (F.erasure a) = F.erasure (FI.close_var x a).
Proof.
  intros; apply~ close_erase_rec.
Qed.

Lemma close_open_rec : forall k x a,
  x \notin fv a ->
  close_var_rec k x (open_trm_wrt_trm_rec k (trm_var_f x) a) = a.
Proof.
  intros. gen k x. induction a; simpls; eauto; intros; fequal~.
  case_if~. subst~. simpls. case_if~.
  case_if~. subst. rewrite notin_singleton in H. false~.
Qed.

Lemma close_open : forall x a,
  x \notin fv a ->
  close_var x (a ^ x) = a.
Proof.
  intros. apply~ close_open_rec.
Qed.

Lemma open_erase_rec : forall k u a,
  open_trm_wrt_trm_rec k (F.erasure u) (F.erasure a) = F.erasure (F.open_trm_wrt_trm_rec k u a).
Proof.
  intros. gen k u. induction a; eauto; simpls; intros; fequal~.
  case_if~; case_if~.
Qed.

Lemma open_erase : forall u a,
  (F.erasure a) ^^ (F.erasure u) = F.erasure (a @@ u).
Proof.
  intros. apply~ open_erase_rec.
Qed.

Lemma erase_pi : forall G AB0 A B S,
    F.erasure AB0 = trm_prod A B ->
    F.typing G AB0 S ->
    exists A1 B0 s, F.erasure AB0 = F.erasure (F.trm_prod A1 B0) /\
                    F.erasure A1 = A /\ F.erasure B0 = B /\
                    F.typing G (F.trm_prod A1 B0) (F.trm_sort s).
Proof.
  induction AB0; intros A B S H; try solve [simpl in H; inversion H].
  - simpl in H; inversion H.
    intros. inversions keep H0. exists AB0_1. exists AB0_2. exists s3. 
    repeat split. inversion H0. subst. auto.
  - simpl in H; inversion H.
    intros H0.  inversions H0.
    destruct (IHAB0_2 A B A0 H) as [A1' [B0' [s' P]]]. auto. destructs P.
    exists A1'. exists B0'. exists s'. simpl in *. subst. repeat split. auto.
    auto.
  - simpl in H; inversion H.
    intros H0.  inversions H0.
    destruct (IHAB0_2 A B A0 H) as [A1' [B0' [s' P]]]. auto. destructs P.
    exists A1'. exists B0'. exists s'. simpl in *. subst. repeat split. auto.
    auto.
Qed.

Require LamI_Full_Dec.
Module FD := LamI_Full_Dec.

Hint Resolve dpara_red_refl.

Lemma beta_to_dpara : forall t1 t2,
  beta t1 t2 -> dpara t1 t2.
Proof.
  introv R. inductions R; autos*.
Qed.

Hint Extern 1 (dpara ?t1 ?t2) => match goal with
  | H: beta t1 t2 |- _ => apply (beta_to_dpara H) 
  end.


Lemma ctyping_sort_inv: forall G s1 e,
  typing G (trm_sort s1) e ->
  exists s2, conv (trm_sort s2) e /\ Ax s1 s2.
Proof.
  intros. inductions H; eauto.
  lets: IHtyping1 s1 __. auto.
  destruct H2 as [s2 ?].
  exists* s2.
Qed.

Lemma sort_erase : forall G A s1 s2,
  F.typing G A (F.trm_sort s2) ->
  F.erasure A = trm_sort s1 ->
  F.typing G (F.trm_sort s1) (F.trm_sort s2).
Proof.
  intros.
  lets: FS.era_typing H.
  unfold FS.era in H1.
  rewrite H0 in H1. simpl in H1.
  destruct (ctyping_sort_inv H1) as [s [? ?]].
  lets: conv_type_type_inv H2.
  subst~.
Qed.

Lemma erasure_sort : forall G a A s,
  F.typing G a A -> F.erasure A = trm_sort s ->
  exists a', F.erasure a = F.erasure a' /\ F.typing G a' (F.trm_sort s).
Proof.
  introv Typ Er.
  destruct (FS.typing_wf_from_typing Typ) as [s1 [? | ?]].
  rewrite H in Er. simpl in Er. inversions Er.
  exists* a.
  exists (F.trm_castup (F.trm_sort s) a).
  splits~. apply* (F.t_castup).
  instantiate(1:=s1).
  apply* sort_erase.
  rewrite Er. simpls~.
Qed.

Lemma erasure_cvt :  forall G a A,
  F.typing G a A -> forall B s, 
  F.typing G B (F.trm_sort s) -> F.erasure A = F.erasure B ->
  exists a', F.erasure a = F.erasure a' /\ F.typing G a' B.
Proof.
  intros.
  exists* (F.trm_castup B a).
  splits~.
  apply* F.t_castup.
  rewrite H1.
  apply~ dpara_red_refl.
  apply~ FS.era_lc_trm.
Qed.

Lemma styping_completeness' :
  forall G a A, styping G a A ->
    forall G0, map F.erasure G0 = G -> F.wf G0 ->
    exists a0 A0,
      F.erasure a0 = a /\
      F.erasure A0 = A /\
      F.typing G0 a0 A0.
Proof.
  intros.
  inductions H.
  (* case: ax *)
  exists* (F.trm_sort s1) (F.trm_sort s2).
  (* case: var *)
  rewrite <- H1 in H0.
  apply binds_map_exists in H0.
  destruct H0 as [a [? ?]]. subst.
  exists* (F.trm_var_f x5) a.
  (* case: abs *)
  destruct~ (IHstyping G0) as [a1 [A1 [EQ1 [EQ2 ?]]]].
  destruct (erasure_sort H7 EQ2) as [a1' [? ?]].
  pick_fresh_gen ((((L \u fv A) \u fv e) \u fv B) \u dom G \u dom G0) x.
  lets P1: H1 x __. auto.
  lets P2: P1 (G0 & x ~ a1') __.
  autorewrite with rew_env_map.
  rewrite H5. rewrite <- H8. rewrite EQ1. auto.
  destruct~ P2 as [e1 [B1 [? [? ?]]]].
  apply F.wf_cons with (s := s1). auto. auto.
  lets Q1: H3 x __. auto.
  lets Q2: Q1 (G0 & x ~ a1') __.
  autorewrite with rew_env_map.
  rewrite H5. rewrite <- H8. rewrite EQ1. auto.
  destruct~ Q2 as [B2 [ss2 [? [? ?]]]].
  destruct (erasure_sort H15 H14) as [B3 [? ?]].
  destruct (erasure_cvt H12 H17) as [e1' [? ?]].
  rewrite <- H16. rewrite H13. rewrite H11. auto.
  exists (F.trm_abs a1' (FI.close_var x e1')).
  exists (F.trm_prod a1' (FI.close_var x B3)).
  splits; simpl; fequals.
  lets: H0 x __. auto.
  rewrite <- close_erase. rewrite <- H18. rewrite H10.
  rewrite~ close_open.
  lets: H0 x __. auto.
  rewrite <- close_erase. rewrite <- H16. rewrite H13.
  rewrite~ close_open.
  gen_eq FR : (((((L \u \{ x}) \u fv A) \u fv e) \u fv B) \u dom G \u dom G0 \u FI.fv (FI.close_var x e1') \u FI.fv (FI.close_var x B3)). intros.
  apply* (@F.t_abs FR); intros; rename x5 into y;
  rewrite EQFR in H20.
  apply (@FD.typing_rename x y G0).
  notin_simpl; apply* FI.close_var_fresh.
  notin_simpl; auto.
  rewrite~ <- (@FI.close_var_open x).
  rewrite~ <- (@FI.close_var_open x).
  apply (@FD.typing_rename_eq x y G0).
  notin_simpl. apply* FI.close_var_fresh.
  simpls~. notin_simpl; auto.
  rewrite~ <- (@FI.close_var_open x).
  (* case: app *)
  destruct~ (IHstyping1 G0) as [a1 [A1 [EQ11 [EQ12 ?]]]].
  destruct~ (IHstyping2 G0) as [a2 [A2 [EQ21 [EQ22 ?]]]].
  destruct (FS.typing_wf_from_typing H3) as [s [? | ?]].
  rewrite H5 in EQ12. simpl in EQ12. inversions EQ12.
  lets P: erase_pi EQ12 H5. destruct P as [A3 [B1 [s2 Q]]].
  destructs Q.
  destruct~ (erasure_cvt H3 H9) as [a1' [? ?]].
  inversions H9.
  destruct~ (erasure_cvt H4 H16) as [a2' [? ?]].
  exists (F.trm_app a1' a2').
  exists (B1 @@ a2').
  splits. simpls. fequal~.
  rewrite <- open_erase. fequals~.
  apply* F.t_app.
  (* case: prod *)
  destruct~ (IHstyping G0) as [a1 [A1 [EQ1 [EQ2 ?]]]].
  destruct (erasure_sort H5 EQ2) as [a1' [? ?]].
  pick_fresh_gen ((((L \u fv A)) \u fv B) \u dom G \u dom G0) x.
  lets P1: H1 x __. auto.
  lets P2: P1 (G0 & x ~ a1') __.
  autorewrite with rew_env_map.
  rewrite H3. rewrite <- H6. rewrite EQ1. auto.
  destruct~ P2 as [B1 [ss2 [? [? ?]]]].
  apply F.wf_cons with (s := s1). auto. auto.
  destruct (erasure_sort H10 H9) as [B1' [? ?]].
  exists (F.trm_prod a1' (FI.close_var x B1')).
  exists (F.trm_sort s3).
  splits; simpl; fequals.
  lets: H0 x __. auto.
  rewrite <- close_erase. rewrite <- H11. rewrite H8.
  rewrite~ close_open.
  gen_eq FR : (((((L \u \{ x}) \u fv A)) \u fv B) \u dom G \u dom G0 \u FI.fv (FI.close_var x a1') \u FI.fv (FI.close_var x B1')). intros.
  apply* (@F.t_prod FR); intros; rename x5 into y;
  rewrite EQFR in H13.
  apply (@FD.typing_rename_eq x y G0).
  notin_simpl. apply* FI.close_var_fresh. simpls~.
  notin_simpl; auto.
  rewrite~ <- (@FI.close_var_open x).
  (* case: mu *)
  destruct~ (IHstyping G0) as [A1 [ss1 [EQ1 [EQ2 ?]]]].
  destruct (erasure_sort H4 EQ2) as [A1' [? ?]].
  pick_fresh_gen ((((L \u fv A)) \u fv e) \u dom G \u dom G0 \u FI.fv A1') x.
  lets P1: H1 x __. auto.
  lets P2: P1 (G0 & x ~ A1') __.
  autorewrite with rew_env_map.
  rewrite H2. rewrite <- H5. rewrite EQ1. auto.
  destruct~ P2 as [a2 [A2 [? [? ?]]]].
  apply F.wf_cons with (s := s). auto. auto.
  rewrite EQ1 in H5. rewrite H5 in H8.
  asserts* K: (F.typing (G0 & x ~ A1') A1' (F.trm_sort s)).
  apply_empty* FS.typing_weaken.
  destruct (erasure_cvt H9 K H8) as [a2' [? ?]].
  exists (F.trm_mu A1' (FI.close_var x a2')).
  exists A1'.
  splits; simpl; fequals.
  lets: H0 x __. auto.
  rewrite <- close_erase. rewrite <- H10. rewrite H7.
  rewrite~ close_open.
  gen_eq FR : (((((L \u \{ x}) \u fv A)) \u fv e) \u dom G \u dom G0 \u FI.fv (FI.close_var x a2') \u FI.fv A1'). intros.
  apply* (@F.t_mu FR). intros; rename x5 into y;
  rewrite EQFR in H12.
  apply (@FD.typing_rename_eq x y G0).
  notin_simpl. apply* FI.close_var_fresh. simpls~.
  notin_simpl; auto.
  rewrite~ <- (@FI.close_var_open x).
  (* case: castup *)
  destruct (IHstyping1 G0 H2 H3) as [a0 [A0 [? [? ?]]]].
  destruct (IHstyping2 G0 H2 H3) as [B1 [ss1 [? [? ?]]]].
  lets P: erasure_sort H9 H8. destruct P as [B1' [? ?]].
  exists (F.trm_castup B1' a0).
  exists B1'. splits~. rewrite <- H7. auto.
  apply* F.t_castup. rewrite <- H10. rewrite H7. rewrite H5. auto.
  (* case: castdn *)
  destruct (IHstyping1 G0 H2 H3) as [a0 [A0 [? [? ?]]]].
  destruct (IHstyping2 G0 H2 H3) as [B1 [ss1 [? [? ?]]]].
  lets P: erasure_sort H9 H8. destruct P as [B1' [? ?]].
  exists (F.trm_castdn B1' a0).
  exists B1'. splits~. rewrite <- H7. auto.
  apply* F.t_castdn. rewrite <- H10. rewrite H7. rewrite H5. auto.
Qed.
  
Lemma swf_to_fwf :
  forall G, swf G ->
    exists G0, map F.erasure G0 = G /\ F.wf G0.
Proof.
  intros. induction G using env_ind.
  exists (empty : F.env). splits~. rewrite~ map_empty.
  inversions H. false* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]].
  subst.
  lets W: proj1 (regular_styping H1).
  lets P: IHG W.
  destruct P as [G0' [K1 K2]].
  lets Q: styping_completeness' H1 G0' K1 K2.
  destruct Q as [a0 [A0 [? [? ?]]]].
  destruct (erasure_sort H4 H3) as [a0' [? ?]].
  exists (G0' & x ~ a0').
  split. autorewrite with rew_env_map.
  rewrite K1. rewrite <- H5. rewrite H. auto.
  apply F.wf_cons with (s := s). auto.
  rewrite <- K1 in H2.
  unfold notin in *.
  rewrite dom_map in H2. auto.
Qed.

Lemma styping_completeness :
  forall G a A, styping G a A ->
    exists G0 a0 A0,
      map F.erasure G0 = G /\
      F.erasure a0 = a /\
      F.erasure A0 = A /\
      F.typing G0 a0 A0.
Proof.
  intros. 
  lets W: proj1 (regular_styping H).
  lets P: swf_to_fwf W.
  destruct P as [G0 [K1 K2]].
  lets Q: styping_completeness' H K1 K2.
  destruct Q as [a1 [A1 R]].
  destructs R.
  exists* G0 a1 A1.
Qed.
