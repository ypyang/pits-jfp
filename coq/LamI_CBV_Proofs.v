Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_CBV_ott LamI_CBV_Infra.
Implicit Types x : var.

Tactic Notation "apply_fresh" "~" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos~.

Lemma typing_weaken : forall G E F t T,
  typing (E & G) t T ->
  wf (E & F & G) ->
  typing (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto 4.
  (* case: var *)
  apply* t_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  instantiate (1 := s3). auto.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0. auto.
  (* case: trm_mu *)
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0.
Qed.

Definition red_out (R : relation) :=
  forall x u t t', value u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Hint Unfold red_out.

Lemma value_red_out :   forall x u t, value u -> value t -> 
  value ([x~>u]t).
Proof.
  intros_all. induction H0; simpl; auto.
  case_if~.
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl; eauto 3.
  rewrite* subst_open.
  apply* r_beta. apply* value_red_out.
  rewrite* subst_open. apply* r_mu. apply* value_red_out.
  apply* r_app_r. apply* value_red_out.
  rewrite* subst_open. apply* r_castdn_mu.
  apply* r_cast_elim. apply* value_red_out.
Qed.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  typing E v V ->
  typing (E & x ~ V & F) t T ->
  value v ->
  typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt Val.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) => 
      forall F, G = E & x ~ V & F -> 
      wf (E & (map (subst x v) F))); 
   intros; subst; simpls subst; eauto 4. 
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ t_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh~ (@t_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
   instantiate (1 := s3). auto.
  (* case: trm_app *)
  apply~ t_app.
  rewrite* subst_open.
  apply~ t_app_v.
  apply~ value_red_out.
  (* case: trm_prod *)
  apply_fresh~ (@t_prod) as y.
   cross; auto. apply_ih_map_bind* H0. auto.
  (* case: trm_prod *)
  apply_fresh~ (@t_mu) as y.
   cross; auto. apply_ih_map_bind* H0.
  (* case: redl *)
  apply~ (@t_castup). apply* reduct_red_out.
  (* case: redr *)
  apply~ (@t_castdn). apply* reduct_red_out.
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
Qed.

Lemma typing_abs_inv : forall E V t P,
  typing E (trm_abs V t) P -> exists T L,
  P = trm_prod V T /\
  forall x, x \notin L -> typing (E & x ~ V) (t ^ x) (T ^ x).
Proof.
  intros. inductions H; eauto 4.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> t --> t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv H2; inversions H2.
  apply* IHvalue.
Qed.

Lemma reduct_determ : forall t t1 t2,
    reduct t t1 -> reduct t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv HH; inversions HH;
  match goal with
  | [ H : trm_abs _ _ --> _ |- _ ] => inversion H
  | [ H : trm_mu _ _ --> _ |- _ ] => inversion H
  | [ H1 : value ?e2, H2 : ?e2 --> _ |- _ ] => false* (@value_cannot_red e2)
  | _ => fequal~
  end.
  inversions H1. false* (@value_cannot_red t2).
  false* (@value_cannot_red (trm_castup A e)).
Qed.

Lemma typing_castup_inv : forall E t U V,
    typing E (trm_castup U t) V -> exists T s,
      typing E U s /\ typing E t T
      /\ U = V /\ U --> T.
Proof.
  introv Typ. gen_eq u: (trm_castup U t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* A.
Qed.

Lemma typing_preserve : forall E t t' T,
  typing E t T -> t --> t' -> typing E t' T.
Proof.
  introv Typ. gen t'.
  induction Typ; introv Red; inversions Red;
    try solve [eauto 4].

  destruct (typing_abs_inv Typ1) as [T' [L1 [EQ Typt2]]].
  inversions EQ.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  asserts P: (T' = T' ^ x).
  apply* open_rec_term.
  asserts Q: ([x~>e2] T' = T').
  apply* subst_fresh.
  rewrite <- Q. clear Q.
  rewrite P. clear P.
  apply_empty~ (@typing_substitution A0).

  inversions Typ1.
  pick_fresh x.
  asserts Q: (B ^^ e2 = B).
  rewrite~ (@subst_intro x B).
  asserts P: (B = B ^ x).
  apply* open_rec_term.
  rewrite <- P.
  apply* subst_fresh.
  rewrite <- Q at 2.
  apply* t_app. rewrite Q.
  gen_eq N: ((trm_prod A B)).
  intros.
  gen_eq M: (trm_mu N e0).
  intros.
  rewrite~ (@subst_intro x e0).
  asserts P: (N = N ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] N = N).
  apply* subst_fresh. rewrite EQN. simpl; auto.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution N).
  subst. apply_fresh* t_mu as y.
  subst; auto.

  destruct (typing_abs_inv Typ1) as [T' [L1 [EQ Typt2]]].
  inversions EQ.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  rewrite~ (@subst_intro x T').
  apply_empty~ (@typing_substitution A0).

  inversions Typ1.
  gen_eq N: ((trm_prod A B)). intros.
  gen_eq M: (trm_mu N e0). intros.
  apply* t_app_v. rewrite <- EQN.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  asserts P: (N = N ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] N = N).
  apply* subst_fresh.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution N).
  subst. apply_fresh* t_mu as y.
  subst; auto.
  
  false* (@value_cannot_red e2).

  inversions Typ.
  apply* t_castdn.
  gen_eq M: (trm_mu A e0). intros.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  asserts P: (A = A ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] A = A).
  apply* subst_fresh.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution A).
  subst. apply_fresh* t_mu as y.
  subst; auto.

  destruct (typing_castup_inv Typ) as [T' [s [TypU [Typt [Eq1 Red1]]]]].
  subst. pose (reduct_determ H Red1).
  rewrite e. auto.
Qed.

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  exists s, typing E U (trm_sort s).
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     exists s.
     apply_empty* typing_weaken.
    destruct (wf_push_inv W).
      destruct~ IHE. exists x1.
      apply_empty* typing_weaken.
Qed.

Lemma typing_prod_inv : forall E U T A,
  typing E (trm_prod U T) A -> 
  exists L s1 s2, typing E U (trm_sort s1) /\
  forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T).
  induction Typ; intros; subst; try solve [false | eauto 4].
  inversions EQP1. exists*.
Qed.


Lemma reduct_fresh : forall A B x,
  A --> B -> x \notin fv A -> x \notin fv B.
Proof.
  intros. induction H; simpls; auto.
  apply* fv_open_term.
  notin_simpl; auto.
  apply* fv_open_term. simpl; auto.
  apply* fv_open_term.
Qed.
  
Lemma typing_fresh' : forall x G e A,
  typing G e A -> x # G -> x \notin fv A.
Proof.
  intros. induction H; simpls; eauto 4.
  apply* notin_fv_from_binds.
  notin_simpl. apply* typing_fresh.
  pick_fresh y. apply* (@fv_open_var y).
  lets: IHtyping1 H0. auto.
  apply* fv_open_term. apply* typing_fresh.
  apply* typing_fresh.
  apply* typing_fresh.
  apply* reduct_fresh.
Qed.

Lemma typing_strengthen : forall F E x A t T,
  typing (E & x ~ A & F) t T ->
  x \notin fv t ->
  wf (E & F) ->
  typing (E & F) t T.
Proof.
  introv Typ N W.
  gen_eq G: (E & x ~ A & F). gen F.
  induction Typ; intros; subst; simpls; eauto 4.
  rewrite notin_singleton in *.
  destruct (binds_middle_inv H0) as [? | [? | ?]].
  apply~ t_var.
  destructs H1. false.
  destructs H1. apply~ t_var.
  (* case: trm_abs *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh* (@t_abs) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  asserts TypS: (typing (E & F & y ~ A0) (e ^ y) (B ^ y)).
  apply_ih_bind* H0; apply* fv_close_var.
  apply_ih_bind* H2.
  apply (typing_fresh' TypS).
  lets W1: H y __. auto.
  lets W2: proj1 (regular_typing W1).
  lets W3: wf_left W2.
  lets W4: ok_from_wf W3.
  destructs (ok_middle_inv W4).
  auto.
  (* case: trm_prod *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  auto.
  (* case: trm_mu *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0; apply* fv_close_var.
Qed.

Lemma typing_wf_from_typing : forall E t T,
  typing E t T ->
  exists s, T = trm_sort s \/ typing E T (trm_sort s).
Proof.
  intros. induction H; eauto 3.
  destruct (typing_wf_from_context H0 H) as [s ?].
  exists* s.
  destruct IHtyping as [s [? | ?]].
  inversions H5. exists s3. right*.
  exists s3. right*.
  destruct IHtyping1 as [s [? | ?]].
  false*.
  destruct (typing_prod_inv H2) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. lets: TypT x __. auto.
  asserts E: (B = B ^ x).
  apply* open_rec_term.
  exists s1.
  rewrite E. right. apply_empty* typing_strengthen.
  rewrite <- E. auto.
  destruct IHtyping1 as [s [? | ?]]. false.
  destruct (typing_prod_inv H2) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. rewrite~ (@subst_intro x).
  exists s1. right.
  unsimpl ([x ~> e2](trm_sort s1)).
  apply_empty* (@typing_substitution A).
  destruct IHtyping as [s [? | ?]].
  subst. inversions H0.
  exists s. right.
  apply (typing_preserve H1). auto.
Qed.

Lemma typing_progress : forall t T, 
  typing empty t T ->
     value t 
  \/ exists t', t --> t'.
Proof.
  introv Typ. lets Typ': Typ. inductions Typ.
  left*.
  false* binds_empty_inv. 
  left*.
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1.
      false* binds_empty_inv. 
      destruct~ IHTyp2. exists* (e ^^ e2).
      destruct H0 as [t' ?].
      exists* (trm_app (trm_abs A e) t').
      destruct~ IHTyp2. exists* (trm_app (e ^^ trm_mu (trm_prod A B) e) e2).
      destruct H2 as [t' ?].
      exists* (trm_app (trm_mu (trm_prod A B) e) t').
      inversions H2. exists* (trm_app t1' e2).
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1.
      false* binds_empty_inv. 
      destruct~ IHTyp2. exists* (e ^^ e2).
      destruct H0 as [t' ?]. false* (@value_cannot_red e2).
      destruct~ IHTyp2. exists* (trm_app (e ^^ trm_mu (trm_prod A B) e) e2).
      destruct H2 as [t' ?]. false* (@value_cannot_red e2).
      inversions H2. exists* (trm_app t1' e2).
  left*.
  left*.
  destruct~ IHTyp2 as [Val2 | [t2' Red2]].
  right. exists* (trm_castup B t2').
  right. destruct~ IHTyp as [Val1 | [t1' Red1]].
      inversions Typ; inversions Val1; try solve [inversion H].
      false* binds_empty_inv. 
      exists* (trm_castdn (e0 ^^ trm_mu A e0)).
      exists* e0.
      exists* (trm_castdn t1').
Qed.

