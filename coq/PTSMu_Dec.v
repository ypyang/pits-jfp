Set Implicit Arguments.
Require Import LibLN.
Require Import PTSMu_ott PTSMu_Infra PTSMu_CR PTSMu_Sound.

Definition decidable (P : Prop) := (P \/ ~ P).

(** Equality of terms *)
Lemma eq_typ_dec : forall (T T' : trm),
  sumbool (T = T') (T <> T').
Proof.
  decide equality.
  decide equality.
  tests EQ: (x5 = x0); auto.
  induction s; induction s0; decide equality.
  decide equality.
Qed.

Lemma typing_app_inv2 : forall G e1 e2 C,
  typing G (trm_app e1 e2) C -> 
  exists A B, typing G e1 (trm_prod A B) /\ conv C (B ^^ e2).
Proof.
  intros. inductions H.
  exists A B. splits*.
  destruct~ (IHtyping1 e1 e2) as [A' [B' [? ?]]].
  exists A' B'. splits*.
Qed.

Lemma beta_star_red_out : red_out (beta star).
Proof.
  apply~ red_all_to_out.
  apply~ beta_star_red_all.
  apply~ star_refl.
Qed.

Lemma beta_star_red_rename : red_rename (beta star).
Proof.
  apply~ red_out_to_rename.
  apply~ red_all_to_out.
  apply~ beta_star_red_all.
  apply~ star_refl.
Qed.

Lemma conv_prod2 : forall L t1 t2 t2',
  lc_trm t1 ->
  (forall x, x \notin L -> conv (t2 ^ x) (t2' ^ x)) ->
  conv (trm_prod t1 t2) (trm_prod t1 t2').
Proof.
  introv R1 R2.
  pick_fresh x. forwards~ Red: (R2 x).
  destruct (church_rosser_beta Red) as [t [Red1 Red2]].
  apply~ (@conv_from_beta_star_trans (trm_prod t1 (close_var x t))).
    apply_fresh* beta_star_prod2 as y.
    apply~ (@beta_star_red_rename x).
    rewrite* <- (@close_var_open x).
    apply~ close_var_fresh.
    apply_fresh* beta_star_prod2 as y.
    apply~ (@beta_star_red_rename x).
    rewrite* <- (@close_var_open x).
    apply~ close_var_fresh.
Qed.

Lemma typing_prod_inv2 : forall E U T P,
  typing E (trm_prod U T) P -> exists L s1 s2 s3,
     conv (trm_sort s3) P
  /\ Rel s1 s2 s3  
  /\ typing E U (trm_sort s1)
  /\ forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T). 
  induction Typ; intros; subst; tryfalse.
  injection EQP1. intros. subst.
  exists* L s1 s2 s3.
  destruct~ (IHTyp1 eq_refl) as [L [s1 [s2 [s3 [EQi [R [TypU P]]]]]]].
  lets Q: (@equiv_trans beta A).
  exists* L s1 s2 s3.
Qed.

Lemma typing_mu_inv2: forall G A e B,
  typing G (trm_mu A e) B -> exists L s,
     conv B A
  /\ typing G A (trm_sort s)
  /\ forall x, x \notin L -> typing (G & x ~ A) (e ^ x) A.
Proof.
  intros. inductions H.
  exists* L s.
  destruct~ (IHtyping1 _ _ eq_refl) as [L' [s' [? [? ?]]]].
  exists* L' s'.
Qed.

Lemma typing_unique : forall E e T1 T2,
  typing E e T1 ->
  typing E e T2 ->
  conv T1 T2.
Proof.
  introv Typ1 Typ2. 
  asserts* W: (lc_trm e).
  gen E T1 T2.
  induction W; introv Typ1 Typ2.

  inductions Typ1; inductions Typ2.
  lets: binds_func H2 H0. subst~.
  apply~ (equiv_trans beta A0).
  apply* IHTyp2_1.
  apply~ (equiv_trans beta A).
  apply* IHTyp1_1.
  apply~ (equiv_trans beta A).
  apply* IHTyp1_1.

  inductions Typ1; inductions Typ2.
  lets: AxUniq H2 H0. subst~.
  apply~ (equiv_trans beta A).
  apply* IHTyp2_1.
  apply~ (equiv_trans beta A).
  apply* IHTyp1_1.
  apply~ (equiv_trans beta A).
  apply* IHTyp1_1.
  
  destruct (typing_app_inv2 Typ1) as [A [B [? ?]]].
  destruct (typing_app_inv2 Typ2) as [A' [B' [? ?]]].
  asserts* W: (lc_trm (trm_app e1 e2)).
  inversions W.
  lets: IHW1 H H1.
  apply~ (equiv_trans beta (B ^^ e2)).
  apply~ (equiv_trans beta (B' ^^ e2)).
  destruct (conv_prod_prod_inv H3) as [C [L P]].
  pick_fresh y.
  rewrite~ (@subst_intro y B).
  rewrite~ (@subst_intro y B').
  apply~ conv_red_out.

  destruct (typing_abs_inv Typ1) as [T' [L1 [s1 [s2 [Typt1 [TypT' [LeP Typt2]]]]]]].
  destruct (typing_abs_inv Typ2) as [T'' [L1' [s1' [s2' [Typt1' [TypT'' [LeP' Typt2']]]]]]].
  apply~ (equiv_trans beta (trm_prod A T')).
  apply~ (equiv_trans beta (trm_prod A T'')).
  apply_fresh* conv_prod2 as y.

  destruct (typing_prod_inv2 Typ1) as [L' [s1 [s2 [s3' [Le [R1 [TypU TypT]]]]]]].
  destruct (typing_prod_inv2 Typ2) as [L'' [s1' [s2' [s3'' [Le' [R2 [TypU' TypT']]]]]]].
  apply~ (equiv_trans beta (trm_sort s3')).
  apply~ (equiv_trans beta (trm_sort s3'')).
  lets P1: IHW TypU TypU'.
  pick_fresh y.
  lets~ P2: H0 y __.
  forwards~ P3: TypT y.
  forwards~ P4: TypT' y.
  lets~ P5: P2 P3 P4.
  lets: conv_type_type_inv P5.
  lets: conv_type_type_inv P1. subst.
  lets: RelUniq R1 R2. subst~.

  destruct~ (typing_mu_inv2 Typ1) as [L1 [s1 [M11 [M12 M13]]]].
  destruct~ (typing_mu_inv2 Typ2) as [L2 [s2 [M21 [M22 M23]]]].
  apply~ (equiv_trans beta A).
Qed.

Lemma typing_app_inv_simple : forall G e1 e2 C,
  typing G (trm_app e1 e2) C -> 
  exists A B, typing G e1 (trm_prod A B).
Proof.
  intros. inductions H; eauto 3.
Qed.

Lemma beta_dec : forall E t T,
  typing E t T ->
  decidable (exists t', beta t t').
Proof.
  introv Typ. lets Typ': Typ. unfold decidable; unfold not.
  inductions Typ; try solve [right; intros [S J]; inversion J].

  destruct~ IHTyp as [[A' W] | NW].
  left. exists* (trm_abs A' e).
  pick_fresh y. lets P: H0 y __. auto.
  lets Q: P __. apply* H.
  destruct Q as [[e' Q] | NQ].
  left. exists (trm_abs A (close_var y e')).
  apply_fresh* beta_abs2 as z.
  apply* (@beta_red_rename y).
  rewrite* <- (@close_var_open y). apply* close_var_fresh.
  right. intros [t' R]. inversions R. apply* NW.
  apply NQ. exists (e' ^ y). pick_fresh z. 
  apply* (@beta_red_rename z).

  destruct~ IHTyp1 as [[e1' P] | NP];
    destruct~ IHTyp2 as [[e2' Q] | NQ]; try solve [left*].
  induction Typ1.
    right. intros [t' R]. inversions R. inversions H5. apply* NQ.
    right. intros [t' R]. inversions R. inversions H5. apply* NQ.
    asserts* W: (lc_trm (trm_app (trm_abs A0 e) e2)).
    inversions W. left. exists* (e ^^ e2).
    right. intros [t' R]. inversions R. apply* NP. apply* NQ.
    false. destruct (typing_app_inv_simple Typ') as [C [D M]].
    destruct (typing_prod_inv M) as [L' [s1' [s2' [s3' [Le [TypU TypT]]]]]].
    apply* conv_type_prod_inv.
    asserts *W: (lc_trm ((trm_app (trm_mu A0 e) e2))).
    inversions W.
    false. apply NP. exists* (e ^^ (trm_mu A0 e)).
    apply* IHTyp1_1.
    
  destruct~ IHTyp as [[A' W] | NW].
  left. exists* (trm_prod A' B).
  pick_fresh y. lets P: H0 y __. auto.
  lets Q: P __. apply* H.
  destruct Q as [[e' Q] | NQ].
  left. exists (trm_prod A (close_var y e')).
  apply_fresh* beta_prod2 as z.
  apply* (@beta_red_rename y).
  rewrite* <- (@close_var_open y). apply* close_var_fresh.
  right. intros [t' R]. inversions R. apply* NW.
  apply NQ. exists (B' ^ y). pick_fresh z. 
  apply* (@beta_red_rename z).

  left. exists* (e ^^ trm_mu A e).
  apply* IHTyp1.
Qed.

(* ********************************************************************** *)
(** Automation *)

Hint Resolve dpara_var dpara_sort dpara_app.

Hint Extern 1 (dpara (trm_abs _ _) (trm_abs _ _)) =>
  let y := fresh "y" in apply_fresh dpara_abs as y.
Hint Extern 1 (dpara (trm_prod _ _) (trm_prod _ _)) =>
  let y := fresh "y" in apply_fresh dpara_prod as y.
Hint Extern 1 (dpara (trm_app (trm_abs _ _) _) (_ ^^ _)) =>
  let y := fresh "y" in apply_fresh dpara_red as y.
Hint Extern 1 (dpara (trm_mu _ _) (trm_mu _ _)) =>
  let y := fresh "y" in apply_fresh dpara_mu as y.
Hint Extern 1 (dpara (trm_mu _ _) (_ ^^ (trm_mu _ _))) =>
  let y := fresh "y" in apply_fresh dpara_mured as y.

(* ********************************************************************** *)
(** Properties of parallel reduction and its iterated version *)

Hint Extern 1 (dpara (if _ == _ then _ else _) _) => case_var.

Lemma dpara_red_refl : red_refl dpara.
Proof.
  intros_all. induction* H. 
Qed.

Lemma dpara_red_out : red_out dpara.
Proof.
  intros_all. induction H0; simpl.
  rewrite* subst_open.
  case_var*. apply *dpara_red_refl.
  apply *dpara_red_refl.
  apply* dpara_app. 
  apply_fresh* dpara_abs as y. cross*. 
  apply_fresh* dpara_prod as y. cross*. 
  rewrite* subst_open.
  apply* dpara_mured.
  apply_fresh* dpara_mu as y. cross*. 
Qed.

Lemma dpara_red_rename : red_rename dpara.
Proof.
  apply* (red_out_to_rename dpara_red_out).
Qed.

Lemma dpara_to_para : forall t t',
    dpara t t' -> para t t'.
Proof.
  intros_all. induction H.
  apply_fresh* para_red as y. apply* para_red_refl. apply* para_red_refl.
  apply* para_red_refl.
  apply* para_red_refl.
  apply* para_app.
  apply* para_abs.
  apply* para_prod.
  apply_fresh* para_mured as y. apply* para_red_refl. apply* para_red_refl.
  apply* para_mu.
Qed.

Hint Extern 1 (para ?t1 ?t2) => match goal with
  | H: dpara t1 t2 |- _ => apply (dpara_to_para H) 
  end.

Lemma dpara_iter_red_refl : red_refl (dpara iter).
Proof.
  intros_all. autos* dpara_red_refl.
Qed.

(* ********************************************************************** *)
(** Equality of decidable parallel reduction *)

Lemma beta_star_to_dpara_iter : 
  (beta star) simulated_by (dpara iter).
Proof.
  intros_all. induction* H. 
  apply* dpara_iter_red_refl.
  apply iter_step. induction H; autos* dpara_red_refl.
Qed.

Lemma para_iter_to_dpara_iter : 
  (para iter) simulated_by (dpara iter).
Proof.
  intros_all.
  pose (para_iter_to_beta_star H).
  apply (beta_star_to_dpara_iter s).
Qed.

Lemma dpara_iter_to_para_iter : 
  (dpara iter) simulated_by (para iter).
Proof.
  intros_all. induction* H.
Qed.

Lemma beta_star_is_dpara_iter : forall e1 e2,
  (beta star) e1 e2 <-> (dpara iter) e1 e2.
Proof.
  intros. unfold iff.
  split; intros.
  apply* beta_star_to_dpara_iter.
  apply* para_iter_to_beta_star.
  apply* dpara_iter_to_para_iter.
Qed.

Lemma dpara_to_conv : forall t t',
    dpara t t' -> conv t t'.
Proof.
  introv D. apply* para_to_conv.
Qed.

Lemma dpara_to_conv' : forall t t',
    dpara t t' -> conv t' t.
Proof.
  introv D. apply* para_to_conv'.
Qed.

Hint Extern 1 (conv ?t1 ?t2) => match goal with
  | H: dpara t1 t2 |- _ => apply (dpara_to_conv H) 
  | H: dpara t2 t1 |- _ => apply (dpara_to_conv' H) 
  end.

(* ********************************************************************** *)
(** Confluence of iterated parallel reduction *)

Lemma dpara_iter_confluence : confluence (dpara iter).
Proof.
  intros_all.
  pose (H1 := dpara_iter_to_para_iter H).
  pose (H2 := dpara_iter_to_para_iter H0).
  destruct (para_iter_confluence H1 H2) as [P' [A B]].
  exists P'. split.
  apply* para_iter_to_dpara_iter.
  apply* para_iter_to_dpara_iter.
Qed.

(* ********************************************************************** *)
(** Subject Reduction *)

Lemma subject_reduction_dpara_iter : 
  subject_reduction (dpara iter).
Proof.
  introv H. induction* H.
  apply* subject_reduction_para_result.
Qed.

Lemma subject_reduction_dpara :
  subject_reduction dpara.
Proof.
  introv H. 
  pose (iter_step dpara t t' H).
  apply* subject_reduction_dpara_iter.
Qed.

Lemma dpara_dec : forall t t',
  lc_trm t ->
  lc_trm t' ->
  decidable (dpara t t').
Proof.  
  introv Typ.  gen t'.
  inductions Typ; intros.
  
  destruct (eq_typ_dec t' (trm_var_f x5)).
  left. rewrite e. apply* dpara_red_refl.
  right. intros P. inversions P. auto.

  destruct (eq_typ_dec t' (trm_sort s)).
  left. rewrite e. apply* dpara_red_refl.
  right. intros P. inversions P. auto.

  asserts* W1: (lc_trm t').
  asserts* W2: (lc_trm e1).
  inductions W1; inductions W2;
  match goal with
  | [|- decidable (dpara (trm_app ?t ?u) (trm_app _ _)) ] =>
    destruct (IHTyp1 _ W1_1);
    destruct (IHTyp2 _ W1_2);
    try solve [left~ | right; intros P; inversions~ P];
    match goal with
    | [ |- decidable (dpara (trm_app (trm_abs ?a ?b) ?c) ?d) ] =>
      destruct (eq_typ_dec (b ^^ c) d) as [Eq | Neq];
        try solve [rewrite <- Eq; left* | right; intros P; inversions~ P]
    end
  | [ |- decidable (dpara (trm_app (trm_abs ?a ?b) ?c) ?d) ] =>
    destruct (eq_typ_dec (b ^^ c) d) as [Eq | Neq];
      match goal with
      | [Eq: _ = _ |- decidable (_) ] => left; rewrite <- Eq; apply* dpara_red
      | [Neq: _ <> _ |- decidable (_) ] => right; intros P; inversions~ P
      end
  | [ |- decidable (dpara (trm_app _ _) _) ] => right; intros P; inversions~ P
  end.

  inductions H1; try solve [right; intros P; inversions~ P].
  destruct (IHTyp _ H1) as [M | M].
  pick_fresh y. lets P1: H0 y __. auto.
  lets P2: P1 __. auto. clear P1.
  lets P3: H2 y __. auto.
  lets P5: P2 P3. clear P3.
  destruct P5 as [Q | Q].
  left. apply_fresh~ dpara_abs as z.
  apply* dpara_red_rename.
  right. intros R. inversions~ R.
  apply Q.  pick_fresh z. apply* dpara_red_rename.
  right. intros R. inversions~ R.
  
  inductions H1; try solve [right; intros P; inversions~ P].
  destruct (IHTyp _ H1) as [M | M].
  pick_fresh y. lets P1: H0 y __. auto.
  lets P3: H2 y __. auto.
  lets P5: P1 P3. clear P3 P1.
  destruct P5 as [Q | Q].
  left. apply_fresh~ dpara_prod as z.
  apply* dpara_red_rename.
  right. intros R. inversions~ R.
  apply Q.  pick_fresh z. apply* dpara_red_rename.
  right. intros R. inversions~ R.

  destruct (eq_typ_dec (e ^^ trm_mu A e) t') as [Eq | Neq].
  left. rewrite <- Eq. apply* dpara_mured.
  asserts* W: (lc_trm t').
  inductions W; try solve [right; intros P; inversions P; auto].
  destruct (IHTyp _ W) as [M | M].
  pick_fresh y. lets P1: H0 y __. auto.
  lets P3: H2 y __. auto.
  lets P5: P1 P3. clear P3 P1.
  destruct P5 as [Q | Q].
  left. apply_fresh~ dpara_mu as z.
  apply* dpara_red_rename.
  right. intros R. inversions~ R.
  apply Q.  pick_fresh z. apply* dpara_red_rename.
  right. intros R. inversions~ R.
Qed.

Lemma dpara_sort_dec : forall E t T s,
  typing E t T ->
  decidable (dpara t (trm_sort s)).
Proof.
  introv Typ.
  induction Typ; try solve [right; intros R; inversions~ R].
  destruct (eq_typ_dec (trm_sort s) (trm_sort s1)).
  left. rewrite e. apply* dpara_red_refl.
  right. intros P. inversions P. auto.
  
  asserts* W: (lc_trm e1).
  inductions W;
  match goal with
  | [ |- decidable (dpara (trm_app (trm_abs ?a ?b) ?c) ?d) ] =>
    destruct (eq_typ_dec (b ^^ c) d) as [Eq | Neq];
      match goal with
      | [Eq: _ = _ |- decidable (_) ] => left; rewrite <- Eq; apply* dpara_red
      | [Neq: _ <> _ |- decidable (_) ] =>   right; intros P; inversions P; auto
      end
  | [ |- decidable (dpara (trm_app _ _) _) ] => right; intros P; inversions P
  end.
  
  destruct (eq_typ_dec (e ^^ trm_mu A e) (trm_sort s)) as [Eq | Neq].
  left. rewrite <- Eq. apply* dpara_mured.
  apply_fresh* lc_trm_mu as y.
  lets: H y __. auto. auto.
  right; intros P; inversions P; auto.

  auto.
Qed.

