Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_Full_ott LamI_Full_Infra.
Implicit Types x : var.

Require PTSMu_Infra PTSMu_CR PTSMu_Sound PTSMu_Dec.
Module CI := PTSMu_Infra.
Module CR := PTSMu_CR.
Module CS := PTSMu_Sound.
Module CD := PTSMu_Dec.
Definition era := erasure.
Definition cera := map erasure.

Tactic Notation "apply_fresh" "~" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos~.

Lemma typing_weaken : forall G E F t T,
  typing (E & G) t T ->
  wf (E & F & G) ->
  typing (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto 4.
  (* case: var *)
  apply* t_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  instantiate (1 := s3). auto.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0. auto.
  (* case: trm_mu *)
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0.
Qed.

Lemma era_subst : forall x u e,
  era ([x ~> u] e) = CI.subst x (era u) (era e).
Proof.
  intros. induction e; simpls; fequals.
  case_if~.
Qed.

Lemma era_open_rec : forall u e n,
  era ({n ~> u} e) = C.open_trm_wrt_trm_rec n (era u) (era e).
Proof.
  intros. gen n. 
  induction e; intros; simpls; fequal*.
  case_if~. case_if~. case_if~.
Qed.

Lemma era_open : forall x e,
    era (e ^ x) = C.open_trm_wrt_trm (era e) (C.trm_var_f x).
Proof.
  intros. lets H: era_open_rec (trm_var_f x) e 0.
  unfold open_trm_wrt_trm.
  unfold C.open_trm_wrt_trm.
  rewrite* H.
Qed.

Lemma era_lc_trm : forall e,
  lc_trm e -> C.lc_trm (era e).
Proof.
  introv W. induction W; simpls; eauto 3.
  apply_fresh* C.lc_trm_abs as y.
  rewrite* <- era_open.
  apply_fresh* C.lc_trm_prod as y.
  rewrite* <- era_open.
  apply_fresh* C.lc_trm_mu as y.
  rewrite* <- era_open.
Qed.

Hint Extern 1 (C.lc_trm (era ?t)) =>
  match goal with
  | H: lc_trm t |- _ => apply~ (@era_lc_trm t)
  end.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  typing E v V ->
  typing (E & x ~ V & F) t T ->
  typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) => 
      forall F, G = E & x ~ V & F -> 
      wf (E & (map (subst x v) F))); 
   intros; subst; simpls subst; eauto 4. 
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ t_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh~ (@t_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
   instantiate (1 := s3). auto.
  (* case: trm_app *)
  rewrite* subst_open.
  (* case: trm_prod *)
  apply_fresh~ (@t_prod) as y.
   cross; auto. apply_ih_map_bind* H0. auto.
  (* case: trm_prod *)
  apply_fresh~ (@t_mu) as y.
   cross; auto. apply_ih_map_bind* H0.
  (* case: redl *)
  apply~ (@t_castup). 
  rewrite era_subst. rewrite era_subst.
  apply* CD.dpara_red_out. apply~ (@era_lc_trm v).
  (* case: redr *)
  apply~ (@t_castdn).
  rewrite era_subst. rewrite era_subst.
  apply* CD.dpara_red_out. apply~ (@era_lc_trm v).
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
Qed.

Lemma typing_abs_inv : forall E V t P,
  typing E (trm_abs V t) P -> exists T L,
  P = trm_prod V T /\
  forall x, x \notin L -> typing (E & x ~ V) (t ^ x) (T ^ x).
Proof.
  intros. inductions H; eauto 4.
Qed.

Lemma typing_castup_inv : forall E t U V,
    typing E (trm_castup U t) V -> exists T s,
      typing E U s /\ typing E t T
      /\ U = V /\ C.dpara (era U) (era T).
Proof.
  introv Typ. gen_eq u: (trm_castup U t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* A.
Qed.

Lemma typing_castdn_inv : forall E t U V,
    typing E (trm_castdn U t) V -> exists T s,
      typing E U s /\ typing E t T
      /\ U = V /\ C.dpara (era T) (era U).
Proof.
  introv Typ. gen_eq u: (trm_castdn U t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* A.
Qed.

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  exists s, typing E U (trm_sort s).
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     exists s.
     apply_empty* typing_weaken.
    destruct (wf_push_inv W).
      destruct~ IHE. exists x1.
      apply_empty* typing_weaken.
Qed.

Lemma typing_prod_inv : forall E U T A,
  typing E (trm_prod U T) A -> 
  exists L s1 s2, typing E U (trm_sort s1) /\
  forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T).
  induction Typ; intros; subst; try solve [false | eauto 4].
  inversions EQP1. exists*.
Qed.

Lemma typing_wf_from_typing : forall E t T,
  typing E t T ->
  exists s, T = trm_sort s \/ typing E T (trm_sort s).
Proof.
  intros. induction H; eauto 3.
  destruct (typing_wf_from_context H0 H) as [s ?].
  exists* s.
  destruct IHtyping as [s [? | ?]].
  inversions H5. exists s3. right*.
  exists s3. right*.
  destruct IHtyping1 as [s [? | ?]].
  false*.
  destruct (typing_prod_inv H1) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. rewrite~ (@subst_intro x).
  exists s1. right.
  unsimpl ([x ~> e2](trm_sort s1)).
  apply_empty* (@typing_substitution A).
Qed.

Lemma era_typing : forall G e T,
  typing G e T ->
  C.typing (cera G) (era e) (era T).
Proof.
  introv Typ.
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      C.typing (cera G) (era t) (era T))
   (P0 := fun (G:env) (W:wf G) => 
      C.wf (cera G)) (e := G); 
   intros; unfold cera; simpls; eauto 4.
  apply_fresh* C.t_abs as y;
    do_rew_all <- era_open autos~;
    rewrite <- map_push.
    apply* H0. apply* H1.
  unfold open_trm_wrt_trm. rewrite era_open_rec.
  apply* C.t_app.
  apply_fresh* C.t_prod as y;
    do_rew_all <- era_open autos~;
    rewrite <- map_push.
    apply* H0.
  apply_fresh* C.t_mu as y;
    do_rew_all <- era_open autos~;
    rewrite <- map_push.
    apply* H0.
  unfold era in *.
  apply C.t_conv with (A := (erasure A)) (s := s).
  auto. auto. apply~ C.equiv_sym.
  apply~ CD.dpara_to_conv.
  unfold era in *.
  apply C.t_conv with (A := (erasure A)) (s := s).
  auto. auto.
  apply~ CD.dpara_to_conv.
  rewrite* map_empty.
  rewrite* map_push.
Qed.

Theorem subject_reduction_result : forall E t t' T,
  typing E t T ->
  C.beta (era t) (era t') ->
  C.typing (cera E) (era t') (era T).
Proof.
  introv Typ Red.
  apply (CS.subject_reduction_result Red).
  apply~ era_typing.
Qed.

Theorem progress_result : forall t T, 
  typing empty t T ->
  C.value (era t) 
  \/ exists t', C.reduct (era t) t'.
Proof.
  introv Typ.
  lets Typ': era_typing Typ. unfolds cera.
  rewrite map_empty in *.
  apply* (CS.progress_era Typ').
Qed.
  
