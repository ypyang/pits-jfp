Set Implicit Arguments.
Require Import LibLN PTSMu_ott PTSMu_Infra.

(* ********************************************************************** *)
(** Inclusion between relations (simulation or a relation by another) *)

Definition simulated (R1 R2 : relation) := 
  forall (t t' : trm), R1 t t' -> R2 t t'.
 
Infix "simulated_by" := simulated (at level 69).

(* ********************************************************************** *)
(** Properties of relations *)

Definition red_regular (R : relation) :=
  forall t t', R t t' -> lc_trm t /\ lc_trm t'.

Definition red_refl (R : relation) :=
  forall t, lc_trm t -> R t t.

Definition red_in (R : relation) :=
  forall t x u u', lc_trm t -> R u u' ->
  R ([x ~> u]t) ([x ~> u']t).
  
Definition red_all (R : relation) :=
  forall x t t', R t t' -> 
  forall u u', R u u' -> 
  R ([x~>u]t) ([x~>u']t').

Definition red_out (R : relation) :=
  forall x u t t', lc_trm u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Definition red_rename (R : relation) :=
  forall x t t' y,
  R (t ^ x) (t' ^ x) -> 
  x \notin (fv t) -> x \notin (fv t') ->
  R (t ^ y) (t' ^ y).

Definition red_through (R : relation) :=
  forall x t1 t2 u1 u2, 
  x \notin (fv t1) -> x \notin (fv u1) ->
  R (t1 ^ x) (u1 ^ x) -> R t2 u2 ->
  R (t1 ^^ t2) (u1 ^^ u2).

(* ********************************************************************** *)
(** ** Generalities on relations *)

Lemma red_all_to_out : forall (R : relation), 
  red_all R -> red_refl R -> red_out R.
Proof.
  intros_all. autos*.
Qed.

Lemma red_out_to_rename : forall (R : relation),
  red_out R -> red_rename R.
Proof.
  intros_all. 
  rewrite* (@subst_intro x t). 
  rewrite* (@subst_intro x t').
Qed.

Lemma red_all_to_through : forall (R : relation),
  red_regular R -> red_all R -> red_through R.
Proof.
  intros_all. lets: (H _ _ H4).
  rewrite* (@subst_intro x t1). 
  rewrite* (@subst_intro x u1).
Qed.  


(* ********************************************************************** *)
(** ** Properties of beta relation *)

Lemma beta_red_out : red_out beta.
Proof.
  intros_all. induction H0; simpl.
  rewrite* subst_open.
  apply* beta_app1. 
  apply* beta_app2.
  apply* beta_abs1.
  apply_fresh* beta_abs2 as y. cross*. 
  apply* beta_prod1.
  apply_fresh* beta_prod2 as y. cross*. 
  rewrite* subst_open.
  apply* beta_mured.
  apply* beta_mu1.
  apply_fresh* beta_mu2 as y. cross*. 
Qed.

Lemma beta_red_rename : red_rename beta.
Proof.
  apply* (red_out_to_rename beta_red_out).
Qed.

(* ********************************************************************** *)
(** ** Properties of beta star relation *)

Lemma beta_star_app1 : forall t1 t1' t2,
  (beta star) t1 t1' -> lc_trm t2 ->
  (beta star) (trm_app t1 t2) (trm_app t1' t2).
Proof.
  intros. induction H. 
  apply* star_refl.
  apply* (@star_trans beta (trm_app t0 t2)).
  apply* star_step.
Qed.

Lemma beta_star_app2 : forall t1 t2 t2',
  (beta star) t2 t2' -> lc_trm t1 ->
  (beta star) (trm_app t1 t2) (trm_app t1 t2').
Proof.
  intros. induction H. 
  apply* star_refl. 
  apply* (@star_trans beta (trm_app t1 t2)).
  apply* star_step.
Qed.

Lemma beta_star_abs1 : forall t1 t1' t2,
  (beta star) t1 t1' -> body t2 ->
  (beta star) (trm_abs t1 t2) (trm_abs t1' t2).
Proof.
  intros. induction H. 
  apply* star_refl. 
  apply* (@star_trans beta (trm_abs t0 t2)). 
  apply* star_step.
Qed.

Lemma beta_star_prod1 : forall t1 t1' t2,
  (beta star) t1 t1' -> body t2 ->
  (beta star) (trm_prod t1 t2) (trm_prod t1' t2).
Proof.
  intros. induction H. 
  apply* star_refl. 
  apply* (@star_trans beta (trm_prod t0 t2)). 
  apply* star_step.
Qed.

Lemma beta_star_mu1 : forall t1 t1' t2,
  (beta star) t1 t1' -> body t2 ->
  (beta star) (trm_mu t1 t2) (trm_mu t1' t2).
Proof.
  intros. induction H. 
  apply* star_refl. 
  apply* (@star_trans beta (trm_mu t0 t2)). 
  apply* star_step.
Qed.

Lemma beta_star_abs2 : forall L t1 t2 t2',  
  lc_trm t1 ->
  (forall x, x \notin L -> (beta star) (t2 ^ x) (t2' ^ x)) ->
  (beta star) (trm_abs t1 t2) (trm_abs t1 t2').
Proof.
  introv R1 R2. pick_fresh x. forwards~ Red: (R2 x).
  assert (body t2).
    exists L. intros y Fry. forwards*: (R2 y).
  assert (body t2').
    exists L. intros y Fry. forwards*: (R2 y).
  gen_eq u: (t2 ^ x). gen_eq u': (t2' ^ x).
  clear R2. gen t2 t2'.
  induction Red; intros; subst. 
  rewrite* (@open_var_inj x t2 t2').
  destruct~ (@close_var_spec t2 x) as [u [P [Q R]]].
   apply* (@star_trans beta (trm_abs t1 u)).
  apply star_step.
   apply_fresh* beta_abs2 as y. 
   apply* (@beta_red_rename x).
Qed. 

Lemma beta_star_prod2 : forall L t1 t2 t2',  
  lc_trm t1 ->
  (forall x, x \notin L -> (beta star) (t2 ^ x) (t2' ^ x)) ->
  (beta star) (trm_prod t1 t2) (trm_prod t1 t2').
Proof.
  introv R1 R2. pick_fresh x. forwards~ Red: (R2 x).
  assert (body t2).
    exists L. intros y Fry. forwards*: (R2 y).
  assert (body t2').
    exists L. intros y Fry. forwards*: (R2 y).
  gen_eq u: (t2 ^ x). gen_eq u': (t2' ^ x).
  clear R2. gen t2 t2'.
  induction Red; intros; subst. 
  rewrite* (@open_var_inj x t2 t2').
  destruct~ (@close_var_spec t2 x) as [u [P [Q R]]].
   apply* (@star_trans beta (trm_prod t1 u)).
  apply star_step.
   apply_fresh* beta_prod2 as y. 
   apply* (@beta_red_rename x).
Qed. 

Lemma beta_star_mu2 : forall L t1 t2 t2',  
  lc_trm t1 ->
  (forall x, x \notin L -> (beta star) (t2 ^ x) (t2' ^ x)) ->
  (beta star) (trm_mu t1 t2) (trm_mu t1 t2').
Proof.
  introv R1 R2. pick_fresh x. forwards~ Red: (R2 x).
  assert (body t2).
    exists L. intros y Fry. forwards*: (R2 y).
  assert (body t2').
    exists L. intros y Fry. forwards*: (R2 y).
  gen_eq u: (t2 ^ x). gen_eq u': (t2' ^ x).
  clear R2. gen t2 t2'.
  induction Red; intros; subst. 
  rewrite* (@open_var_inj x t2 t2').
  destruct~ (@close_var_spec t2 x) as [u [P [Q R]]].
   apply* (@star_trans beta (trm_mu t1 u)).
  apply star_step.
   apply_fresh* beta_mu2 as y. 
   apply* (@beta_red_rename x).
Qed.

Lemma beta_star_red_in : red_in (beta star).
Proof.
  introv Wf Red. lets: lc_trm. induction Wf; simpl.
  case_var*.
  autos~.
  apply~ (@star_trans beta (trm_app ([x ~> u']e1) ([x ~> u]e2))).
    apply* beta_star_app1.
    apply* beta_star_app2.
  autos*.
  apply~ (@star_trans beta (trm_abs ([x ~> u']A) ([x ~> u]e))).
    apply* beta_star_abs1.
    apply* (@beta_star_abs2 (L \u \{x})). intros y Fr. cross*.
  apply~ (@star_trans beta (trm_prod ([x ~> u']A) ([x ~> u]B))).
    apply* beta_star_prod1.
    apply* (@beta_star_prod2 (L \u \{x})). intros y Fr. cross*.
  apply~ (@star_trans beta (trm_mu ([x ~> u']A) ([x ~> u]e))).
    apply* beta_star_mu1.
    apply* (@beta_star_mu2 (L \u \{x})). intros y Fr. cross*.  
Qed.

Lemma beta_star_red_all : red_all (beta star).
Proof.
  introv Redt. induction Redt; simpl; intros u u' Redu. 
  apply* beta_star_red_in.
  apply* (@star_trans beta ([x ~> u]t2)).
  apply* (@star_trans beta ([x ~> u]t')).
   apply* star_step. apply* beta_red_out.
   apply* beta_star_red_in.
Qed.

Lemma beta_star_red_through : red_through (beta star).
Proof.
  apply* (red_all_to_through red_regular_beta_star beta_star_red_all).
Qed.

(* ********************************************************************** *)
(** ** Additional Definitions Used in this Proof *)

(* ********************************************************************** *)
(** ** Lemmas Associated to Additional Definitions *)

Hint Constructors para iter_.

(* ********************************************************************** *)
(** Regularity *)

Lemma red_regular_para : red_regular para.
Proof.
  intros_all. induction* H.
Qed.

Lemma red_regular_para_iter : red_regular (para iter).
Proof.
  intros_all. induction* H. apply* red_regular_para.
Qed.

Lemma red_regular_dpara : red_regular dpara.
Proof.
  intros_all. induction* H.
Qed.

Hint Extern 1 (lc_trm ?t) => match goal with
  | H: para t _ |- _ => apply (proj1 (red_regular_para H))
  | H: para _ t |- _ => apply (proj2 (red_regular_para H))
  | H: (para iter) t _ |- _ => apply (proj1 (red_regular_para_iter))
  | H: (para iter) _ t |- _ => apply (proj2 (red_regular_para_iter))
  | H: dpara t _ |- _ => apply (proj1 (red_regular_dpara H))
  | H: dpara _ t |- _ => apply (proj2 (red_regular_dpara H))
  end.

(* ********************************************************************** *)
(** Automation *)

Hint Resolve para_var para_sort para_app.

Hint Extern 1 (para (trm_abs _ _) (trm_abs _ _)) =>
  let y := fresh "y" in apply_fresh para_abs as y.
Hint Extern 1 (para (trm_prod _ _) (trm_prod _ _)) =>
  let y := fresh "y" in apply_fresh para_prod as y.
Hint Extern 1 (para (trm_app (trm_abs _ _) _) (_ ^^ _)) =>
  let y := fresh "y" in apply_fresh para_red as y.
Hint Extern 1 (para (trm_mu _ _) (trm_mu _ _)) =>
  let y := fresh "y" in apply_fresh para_mu as y.
Hint Extern 1 (para (trm_mu _ _) (_ ^^ (trm_mu _ _))) =>
  let y := fresh "y" in apply_fresh para_mured as y.

(* ********************************************************************** *)
(** Properties of parallel reduction and its iterated version *)

Section ParaProperties.

Hint Extern 1 (para (if _ == _ then _ else _) _) => case_var.

Lemma para_red_all : red_all para.
Proof.
  intros x t t' H. induction H; intros; simple*.
  rewrite* subst_open. apply_fresh* para_red as y. cross*.
  case_var*.
  apply_fresh* para_abs as y. cross*.
  apply_fresh* para_prod as y. cross*.
  rewrite* subst_open. apply_fresh* para_mured as y.
  fold subst_trm. cross*.
  asserts* P: ((subst_trm u' x e' ^ y) = ([x ~> u'] e' ^ y)).
  rewrite* <- subst_open_var.
  rewrite P. apply* H1.
  apply_fresh* para_mu as y. cross*.
Qed.

Lemma para_red_refl : red_refl para.
Proof.
  intros_all. induction* H. 
Qed.

Lemma para_red_out : red_out para.
Proof.
  apply* (red_all_to_out para_red_all para_red_refl). 
Qed.

Lemma para_red_rename : red_rename para.
Proof.
  apply* (red_out_to_rename para_red_out).
Qed.

Lemma para_red_through : red_through para.
Proof.
  apply* (red_all_to_through red_regular_para para_red_all).
Qed.

Lemma para_iter_red_refl : red_refl (para iter).
Proof.
  intros_all. autos* para_red_refl.
Qed.

End ParaProperties.

(* ********************************************************************** *)
(** Equality of beta star and iterated parallel reductions *)

Lemma beta_star_to_para_iter : 
  (beta star) simulated_by (para iter).
Proof.
  intros_all. induction* H. 
  apply* para_iter_red_refl.
  apply iter_step. induction H; autos* para_red_refl.
Qed.

Lemma para_iter_to_beta_star : 
  (para iter) simulated_by (beta star).
Proof.
  intros_all. induction H; eauto.
  induction H.
  applys~ (@star_trans beta (e1 ^^ e2)).
   pick_fresh x. apply* (@beta_star_red_through x).
  apply* star_refl.
  apply* star_refl.
  apply~ (@star_trans beta (trm_app e1' e2)).
   apply* beta_star_app1. apply* beta_star_app2.
  apply~ (@star_trans beta (trm_abs A' e)).
    apply* beta_star_abs1. apply* beta_star_abs2.
  apply~ (@star_trans beta (trm_prod A' B)).
    apply* beta_star_prod1. apply* beta_star_prod2.
  applys~ (@star_trans beta (e ^^ trm_mu A e)).
   pick_fresh x. apply* (@beta_star_red_through x).
   apply~ (@star_trans beta (trm_mu A' e)).
   apply* beta_star_mu1. apply* beta_star_mu2.
  apply~ (@star_trans beta (trm_mu A' e)).
   apply* beta_star_mu1. apply* beta_star_mu2.
Qed.

(* ********************************************************************** *)
(** ** Proof of Church-Rosser Property for Beta Reduction *)

(* ********************************************************************** *)
(** Confluence of parallel reduction *)

Lemma para_abs_inv : forall t1 t2 u,
  para (trm_abs t1 t2) u -> exists L t1' t2', 
  u = (trm_abs t1' t2') /\ para t1 t1' /\
  forall x : var, x \notin L -> para (t2 ^ x) (t2' ^ x).
intros. inversion H. exists* L.
Qed.

Lemma para_prod_inv : forall t1 t2 u,
  para (trm_prod t1 t2) u -> exists L t1' t2', 
  u = (trm_prod t1' t2') /\ para t1 t1' /\
  forall x : var, x \notin L -> para (t2 ^ x) (t2' ^ x).
Proof.
  intros. inversion H. exists* L.
Qed.

(** Confluence property (proved for beta star) *)

Definition confluence (R : relation) := 
  forall M S T, R M S -> R M T -> 
  exists P, R S P /\ R T P. 

(** Church-Rosser property (proved for beta) *)

Definition church_rosser (R : relation) :=
  forall t1 t2, (R equiv) t1 t2 -> 
  exists t, (R star) t1 t /\ (R star) t2 t.

Lemma para_confluence : confluence para.
Proof.
  introv HS. gen T. induction HS; intros T HT; inversions HT.
    (* case: red / red *)
  lets~ [u2 [U2a U2b]]: IHHS e2'0 __. clear IHHS.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H1 (e1'0 ^ x) __. auto. clear H1. 
  destruct~ (@close_var_spec u1x x) as [u1 [EQu1 termu1]].
  rewrite EQu1 in U1a, U1b. 
  exists (u1 ^^ u2). split; apply* (@para_red_through x).
    (* case: red / trm_app *)
  lets~ [u2 [U2a U2b]]: IHHS e2'0 __. clear IHHS.
  lets [L2 [t1'0x [t2'0x [EQ [Ht1'0 Ht2'0]]]]]: (para_abs_inv H4).
  rewrite EQ in H4 |- *.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H1 (t2'0x ^ x) __. auto.
  lets~ [u1 [EQu1 termu1]]: (@close_var_spec u1x x) __.
  rewrite EQu1 in U1a, U1b.
  exists (u1 ^^ u2). split. 
    apply* (@para_red_through x). 
    apply_fresh para_red as y; autos*. 
    apply* (@para_red_rename x).
    (* case: var / var *)
  autos*.
    (* case: srt / srt *)
  autos*.
    (* case: trm_app / red *)
  lets~ [u2 [U2a U2b]]: IHHS2 e2'0 __. clear IHHS2.
  lets [L2 [t1'x [t2'x [EQ [Ht1'x Ht2'x]]]]]: (para_abs_inv HS1).
  lets~ [u1x [U1a U1b]]: (IHHS1 (trm_abs t1'x e1'0)) __. clear IHHS1.
  rewrite EQ in HS1, U1a |- *.
  lets~ [L1 [v1 [v2 [EQ' [Hv1 Hv2]]]]]: (para_abs_inv U1b). 
  rewrite EQ' in U1a, U1b.
  exists (v2 ^^ u2). split.
    inversions U1a. 
    apply* (@para_red L0).
    pick_fresh x. apply* (@para_red_through x).
    (* case: trm_app / trm_app *)
  lets~ [P1 [HP11 HP12]]: (IHHS1 e1'0) __. 
  lets~ [P2 [HP21 HP22]]: (IHHS2 e2'0) __. 
  exists* (trm_app P1 P2). 
    (* case: trm_abs / trm_abs *)
  pick_fresh x. lets~ [px [P0 P1]]: H0 (e'0 ^ x) __. auto.
  lets~ [u1 [HP1 HP2]]: (IHHS A'0) __. clear H0 IHHS.
  lets~ [p [EQP termp]]: (@close_var_spec px x) __.
  rewrite EQP in P0, P1.
  exists (trm_abs u1 p). split; 
   apply_fresh* para_abs as y; apply* (@para_red_rename x). 
    (* case: trm_prod / trm_prod *)
  pick_fresh x. lets~ [px [P0 P1]]: H0 (B'0 ^ x) __. auto. 
  lets~ [u1 [HP1 HP2]]: IHHS A'0 __. clear H0 IHHS.
  lets~ [p [EQP termp]]: (@close_var_spec px x) __.
  rewrite EQP in P0, P1.
  exists (trm_prod u1 p). split; 
   apply_fresh* para_prod as y; apply* (@para_red_rename x).
    (* case: mured / mured *)
  lets~ [u2 [U2a U2b]]: IHHS A'0 __. clear IHHS.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H0 (e'0 ^ x) __. auto. clear H0.
  destruct~ (@close_var_spec u1x x) as [u1 [EQu1 termu1]].
  rewrite EQu1 in U1a, U1b.
  exists (u1 ^^ trm_mu u2 u1). split; apply* (@para_red_through x).
  apply_fresh* para_mu as y; apply* (@para_red_rename x).
  apply_fresh* para_mu as y; apply* (@para_red_rename x).
    (* case: mured / mu *)
  lets~ [u2 [U2a U2b]]: IHHS A'0 __. clear IHHS.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H0 (e'0 ^ x) __. auto. clear H0.
  destruct~ (@close_var_spec u1x x) as [u1 [EQu1 termu1]].
  rewrite EQu1 in U1a, U1b.
  exists (u1 ^^ trm_mu u2 u1).
  split. apply* (@para_red_through x).
    apply_fresh* para_mu as y; apply* (@para_red_rename x).
    apply_fresh* para_mured as y; apply* (@para_red_rename x).
    (* case: mu / mured *)
  lets~ [u2 [U2a U2b]]: IHHS A'0 __. clear IHHS.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H0 (e'0 ^ x) __. auto. clear H0.
  destruct~ (@close_var_spec u1x x) as [u1 [EQu1 termu1]].
  rewrite EQu1 in U1a, U1b.
  exists (u1 ^^ trm_mu u2 u1).
  split. 
    apply_fresh* para_mured as y; apply* (@para_red_rename x).
    apply* (@para_red_through x).
    apply_fresh* para_mu as y; apply* (@para_red_rename x).
    (* case: mu / mu *)
  lets~ [u2 [U2a U2b]]: IHHS A'0 __. clear IHHS.
  pick_fresh x. lets~ [u1x [U1a U1b]]: H0 (e'0 ^ x) __. auto. clear H0.
  destruct~ (@close_var_spec u1x x) as [u1 [EQu1 termu1]].
  rewrite EQu1 in U1a, U1b.
  exists (trm_mu u2 u1).
  split; apply_fresh* para_mu as y; apply* (@para_red_rename x).
Qed.

(* ********************************************************************** *)
(** Confluence of iterated parallel reduction *)

Lemma para_iter_parallelogram : 
  forall M S, (para iter) M S -> forall T, para M T ->
  exists P, para S P /\ (para iter) T P. 
Proof.
  introv H. induction H; intros T MtoT.
  destruct~ (IHiter_1 T) as [P [HP1 HP2]]. 
   destruct~ (IHiter_2 P) as [Q [HQ1 HQ2]].
   exists Q. autos* (@iter_trans para P).
  destruct* (para_confluence H MtoT).
Qed.

Lemma para_iter_confluence : confluence (para iter).
Proof.
  introv MtoS MtoT. gen T.
  induction MtoS; intros T MtoT.
  destruct~ (IHMtoS1 T) as [P [HP1 HP2]]. 
   destruct~ (IHMtoS2 P) as [Q [HQ1 HQ2]]. exists* Q. 
  destruct* (para_iter_parallelogram MtoT H).
Qed.

(* ********************************************************************** *)
(** Church-Rosser property of beta reduction *)

Lemma confluence_beta_star : confluence (beta star).
Proof.
  intros_all. destruct (@para_iter_confluence M S T).
  autos* beta_star_to_para_iter.
  autos* beta_star_to_para_iter.
  autos* para_iter_to_beta_star.
Qed.

Lemma church_rosser_beta : church_rosser beta.
Proof.
  introv H. induction H.
  exists* t.
  destruct* IHequiv_.
  destruct IHequiv_1 as [u [Pu Qu]].
   destruct IHequiv_2 as [v [Pv Qv]].
   destruct (confluence_beta_star Qu Pv) as [w [Pw Qw]].
   exists w. split.
     apply* (@star_trans beta u).
     apply* (@star_trans beta v).
  exists* t'.
Qed.

(* ********************************************************************** *)
(** ** Some Properties of Conversion *)

Lemma conv_red_out : red_out conv.
Proof.
  intros_all. lets: beta_red_out. induction* H0.
Qed.

Lemma conv_from_beta_star : 
  (beta star) simulated_by (conv).
Proof.
  intros_all. induction* H.
Qed.

Lemma conv_from_beta_star_trans : forall T U1 U2,
  (beta star) U1 T -> (beta star) U2 T -> conv U1 U2.
Proof.
 introv R1 R2. apply (@equiv_trans beta T).
   apply* conv_from_beta_star.
   apply equiv_sym. apply* conv_from_beta_star.
Qed.

Lemma conv_from_open_beta : forall u u' t,
  body t -> beta u u' -> conv (t ^^ u') (t ^^ u).
Proof.
  introv B R. destruct B as [L Fr].
  pick_fresh x.
  rewrite* (@subst_intro x t u).
  rewrite* (@subst_intro x t u').
  unfold conv. apply equiv_sym.
  apply conv_from_beta_star.
  apply* beta_star_red_in.
Qed.

Lemma para_to_conv : forall t t',
    para t t' -> conv t t'.
Proof.
  intros.
  assert ((para iter) t t').
  apply* iter_step.
  assert ((beta star) t t').
  apply* para_iter_to_beta_star.
  apply* conv_from_beta_star.
Qed.

Lemma para_to_conv' : forall t t',
    para t t' -> conv t' t.
Proof.
  intros.
  assert ((para iter) t t').
  apply* iter_step.
  assert ((beta star) t t').
  apply* para_iter_to_beta_star.
  apply~ equiv_sym.
  apply* conv_from_beta_star.
Qed.

Hint Extern 1 (conv ?t1 ?t2) => match goal with
  | H: para t1 t2 |- _ => apply (para_to_conv H) 
  | H: para t2 t1 |- _ => apply (para_to_conv' H) 
  end.

(* ********************************************************************** *)
(** ** Inversion Lemmas for Conversion *)

Section ProdInv.

Tactic Notation "helper" :=
 match goal with |- ex (fun _ => ex (fun _ => 
  trm_prod ?A ?B = trm_prod _ _ /\ _)) =>
  exists A B; splits 3; [auto | | exists_fresh ] end.

Tactic Notation "helper" "*" := helper; eauto.

Lemma beta_star_prod_any_inv : forall P U1 T1,
  (beta star) (trm_prod U1 T1) P ->
  exists U2, exists T2, P = trm_prod U2 T2 /\
  (beta star) U1 U2 /\
  exists L, forall x, x \notin L ->
   (beta star) (T1 ^ x) (T2 ^ x).
Proof.
  introv H. gen_eq Q: (trm_prod U1 T1). gen U1 T1.
  induction H; intros; subst.
  inversions H. helper*.
  destruct~ (IHstar_1 U1 T1) as [U3 [T3 [EQ3 [H3 [L3 R3]]]]]. subst.
   destruct~ (IHstar_2 U3 T3) as [U2 [T2 [EQ2 [H2 [L2 R2]]]]]. subst.
   helper*.
   inversions H; helper*.
Qed.

End ProdInv.

Lemma beta_star_type_any_inv : forall P s,
  (beta star) (trm_sort s) P -> P = trm_sort s.
Proof.
  introv R.
  gen_eq T: (trm_sort s). 
  induction R; intros EQ; subst.
  auto.
  forwards*: IHR1. subst. auto.
  inversion H.
Qed.

Lemma conv_prod_prod_inv : forall U1 T1 U2 T2,
  conv (trm_prod U1 T1) (trm_prod U2 T2) ->
     conv U1 U2
  /\ exists L, forall x, x \notin L -> conv (T1 ^ x) (T2 ^ x).
Proof.
  unfold conv. introv C.
  destruct (church_rosser_beta C) as [P3 [Red1 Red2]].
  destruct (beta_star_prod_any_inv Red1)
   as [P3_11 [P3_12 [EQ1 [R1 [L1 S1]]]]].
  destruct (beta_star_prod_any_inv Red2)
   as [P3_21 [P3_22 [EQ2 [R2 [L2 S2]]]]].
  subst. inversions EQ2.
  split. applys conv_from_beta_star_trans R1 R2.
  exists_fresh. intros x Fr.
   forwards~ K1: (S1 x). forwards~ K2: (S2 x).
   apply* conv_from_beta_star_trans.
Qed.

Lemma conv_type_type_inv : forall s1 s2,
  conv (trm_sort s1) (trm_sort s2) -> s1 = s2.
Proof.
  unfold conv. introv C.
  destruct (church_rosser_beta C) as [T [Red1 Red2]].
  rewrite (beta_star_type_any_inv Red1) in Red2.
  lets K: (beta_star_type_any_inv Red2). inversions* K.
Qed.

Lemma conv_type_prod_inv : forall s U1 U2,
  conv (trm_sort s) (trm_prod U1 U2) -> False.
Proof.
  unfold conv. introv C.
  destruct (church_rosser_beta C) as [P3 [Red1 Red2]].
  destruct (beta_star_prod_any_inv Red2)
     as [P3_11 [P3_12 [EQ1 [R1 [L1 S1]]]]].
  rewrite (beta_star_type_any_inv Red1) in *.
  false.
Qed.

